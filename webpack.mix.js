const mix = require('laravel-mix');
const MonacoEditorPlugin = require('monaco-editor-webpack-plugin');
const monacoEditorPlugin = new MonacoEditorPlugin({
    languages: ['javascript','css','html','php','markdown','typescript','sql','scss','xml'], // Remove for production?
});
const CompressionPlugin = require("compression-webpack-plugin");
const zlib = require('zlib');
const path = require('path');


const plugins = [
    monacoEditorPlugin,
];

if(process.env.NODE_ENV === 'production'){
    plugins.push(new CompressionPlugin({
        test: /\.(js|css|html|svg)$/,
    }));
    plugins.push(new CompressionPlugin({
        filename: '[path][base].br',
        algorithm: 'brotliCompress',
        test: /\.(js|css|html|svg)$/,
        compressionOptions: {
            params: {
                [zlib.constants.BROTLI_PARAM_QUALITY]: 10,
            }
        },
        //threshold: 10240,
        minRatio: 0.8,
        deleteOriginalAssets: false,
    }));
}

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
        require('tailwindcss'),
    ]).webpackConfig({
        plugins,
    });


