<?php

namespace Database\Factories;

use App\Models\DisplayRegistration;
use Illuminate\Database\Eloquent\Factories\Factory;

class DisplayRegistrationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DisplayRegistration::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => $this->generateUniqueCode(7),
            'last_seen_ip' => '127.0.0.1',
            'heartbeat_at' => now(),
        ];
    }

    // Copied from Http\Controllers\Client\AuthController
    private function generateUniqueCode($length){
        for($try = 0; $try < 10; ++$try){
            $chars = "ABCDEFGHJKMNPQRSTUVWXYZ23456789@#$%&";
            $ret = "";
            $bytes = unpack('C*',random_bytes($length));
            foreach($bytes as $byte){
                $index = $byte % strlen($chars);
                $ret .= $chars[$index];
            }
            // Check if code exists
            // Skipping since this is a factory for TESTING
            //if(DisplayRegistration::where('code',$ret)->count() == 0) return $ret;
        }
        return $ret;
    }
}
