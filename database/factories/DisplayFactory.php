<?php

namespace Database\Factories;

use App\Models\Display;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class DisplayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Display::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(3,true),
            'status' => 'New',
            'join_user_id' => User::factory()->create()->id,
            //'description' => $this->faker->sentence(),
        ];
    }
}
