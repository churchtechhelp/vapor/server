<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShowTouchEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('show_touch_events', function (Blueprint $table) {
            $table->id();
            $table->foreignId('show');
            $table->string('file_viewed')->nullable();
            $table->integer('height')->nullable();
            $table->integer('width')->nullable();
            $table->integer('x')->nullable();
            $table->integer('y')->nullable();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('show_touch_events');
    }
}
