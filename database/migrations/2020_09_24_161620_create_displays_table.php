<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisplaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('displays', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('join_user_id');
            $table->foreignId('show_id')->nullable();
            $table->foreignId('inventory_id')->nullable();
            //$table->foreignId('current_reg_id')->nullable();
            $table->foreignId('display_group_id')->nullable();
            $table->foreignId('group_id')->nullable();
            $table->string('status')->default("Never Connected");
            $table->timestamps();
        });

        Schema::create('display_groups', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('group_id');
            $table->timestamps();
        });

        Schema::create('display_registrations', function (Blueprint $table) {
            $table->id();
            $table->string('code',10);
            $table->rememberToken();
            $table->timestamp('remember_token_age')->nullable();
            $table->ipAddress('last_seen_ip');
            $table->foreignId('display_id')->nullable();
            $table->timestamp('heartbeat_at');
            $table->timestamp('assigned_at')->nullable();
            $table->foreignId('assigned_user_id')->nullable();
            $table->timestamp('invalidated_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('displays');
        Schema::dropIfExists('display_groups');
        Schema::dropIfExists('display_registrations');
    }
}
