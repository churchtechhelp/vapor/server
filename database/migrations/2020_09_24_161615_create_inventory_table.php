<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->string('picture_id');
            $table->string('inventory_model_id');
            $table->string('serial');
            $table->foreignId('group_id');
            $table->boolean('multicheckout')->default(false);
            $table->timestamps();
        });

        Schema::create('inventory_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('inventory_models', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('inventory_type_id');
            $table->string('inventory_manufacturer_id');
            $table->string('image_path');
            $table->timestamps();
        });

        Schema::create('inventory_manufacturers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('logo_path');
            $table->timestamps();
        });

        Schema::create('inventory_checkouts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('inventory_id');
            $table->foreignId('user_id');
            $table->integer('status')->default(0); // 0=reserved,1=out,2=in,8=late,9=denied
            $table->timestamp('reserved_from')->nullable();
            $table->timestamp('reserved_to')->nullable();
            $table->foreignId('approved_user_id')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->foreignId('checkout_user_id');
            $table->timestamp('checkout_at');
            $table->timestamp('checkout_due')->nullable();
            $table->foreignId('checkin_user_id')->nullable();
            $table->timestamp('checkin_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
        Schema::dropIfExists('inventory_types');
        Schema::dropIfExists('inventory_checkouts');
    }
}
