<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId("parent_id")->nullable();
            $table->foreignId("primary_user_id");
            $table->string('domain')->nullable();
            $table->boolean('organization')->default(false);
            $table->string('features')->nullable();
            $table->timestamps();
        });

        Schema::create('group_user', function (Blueprint $table) {
            $table->foreignId('user_id');
            $table->foreignId('group_id');
            $table->integer('role')->default(0);
        });

        /*foreach (['file','display','show','inventory'] as $model) {
            if(strcmp($model,'group') > 0) $tablename = 'group_' . $model;
            else $tablename = $model . '_group';
            Schema::create($tablename, function (Blueprint $table) {
                $table->foreignId($model . '_id');
                $table->foreignId('group_id');
            });
        }*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_user');
        /*foreach (['file','display','show','inventory'] as $model) {
            if(strcmp($model,'group') > 0) $tablename = 'group_' . $model;
            else $tablename = $model . '_group';
            Schema::dropIfExists($tablename);
        }*/
    }
}
