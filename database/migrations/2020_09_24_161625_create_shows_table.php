<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shows', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->boolean('ready')->default('false');
            $table->jsonb('config');
            $table->foreignId('creator_user_id')->nullable();
            $table->timestamp('archived_at')->nullable();
            $table->foreignId('archived_user_id')->nullable();
            $table->timestamp('last_used_at')->nullable();
            $table->foreignId('group_id')->nullable();
            $table->timestamps();
        });

        Schema::create('show_user', function (Blueprint $table) {
            $table->foreignId('show_id');
            $table->foreignId('user_id');
        });

        Schema::create('group_show', function (Blueprint $table) {
            $table->foreignId('show_id');
            $table->foreignId('group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shows');
    }
}
