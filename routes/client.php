<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Client\DisplayController;
//use App\Http\Controllers\Client\ShowController;
use App\Http\Controllers\Client\AuthController;
use Illuminate\Broadcasting\BroadcastController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('/check', 'ClientController@check');
Route::post('register', [AuthController::class, 'register']);
Route::middleware("auth:client")->group(function(){
    Route::post("config", [DisplayController::class, 'config']);
    Route::post("status", [DisplayController::class, 'status']);
    Route::post('files/{path}', [DisplayController::class, 'file'])->where('path', '.*');
    Route::post('broadcasting/auth', [BroadcastController::class, 'authenticate']);
});