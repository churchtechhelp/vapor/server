<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DisplayController;
use App\Http\Controllers\ShowController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\HelpController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/about', function(){
    return view('about');
})->name('about');

Route::group(['middleware' => config('fortify.middleware', ['web'])], function () {
    $limiter = config('fortify.limiters.login');

    Route::post('/login', [LoginController::class, 'store'])
        ->middleware(array_filter([
            'guest',
            $limiter ? 'throttle:'.$limiter : null,
        ]));
    Route::post('/register', [RegistrationController::class, 'store'])->middleware(['guest']);
});

Route::get('/terms',function(){
    return view('terms');
})->name('terms');

Route::middleware(['auth:sanctum', 'verified'])->group(function(){
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    
    // Display Routes
    Route::prefix('displays')->group(function(){
        Route::get('/', [DisplayController::class, 'index'])->name('displays');
        Route::post('/', [DisplayController::class, 'create']);
        Route::post('search/{search?}', [DisplayController::class, 'search']);
        Route::get('/{display}', [DisplayController::class, 'show']);
        Route::delete('/{display}', [DisplayController::class, 'delete']);
        Route::post('/{display}/registrations', [DisplayController::class, 'register']);
        Route::delete('/{display}/registrations/{registration}', [DisplayController::class, 'unregister']);
    });
    Route::prefix('shows')->group(function(){
        Route::get('/', [ShowController::class, 'index'])->name('show');
        Route::post('/',[ShowController::class, 'store']);
        Route::post('search/{search?}', [ShowController::class, 'search']);
        Route::get('/{show}', [ShowController::class, 'show']);
        Route::put('/{show}', [ShowController::class, 'update']);
        Route::post('/{show}/display/{display}', [ShowController::class, 'deploy']);
        Route::delete('/{show}/display/{display}', [ShowController::class, 'deleteDeployment']);
    });
    Route::prefix('inventory')->group(function(){
        Route::get('/', [InventoryController::class, 'index'])->name('inventory');
        Route::post('search/{search?}', [DisplayController::class, 'search']);

    });
    Route::prefix('files')->group(function(){
        Route::get('/', [FileController::class, 'index'])->name('files');
        Route::post('/', [FileController::class, 'store']);
        Route::post('{user}/', [FileController::class, 'uploadUser']);
        // Organization/Group files
        Route::post('group/{group}/{path}',[FileController::class, 'uploadGroup'])->where('group','[0-9]+')->where('path', '.*');
        Route::get('group/{group}/{path?}',[FileController::class, 'showGroup'])->where('group','[0-9]+')->where('path', '.*');
        Route::put( 'group/{group}/{path}',[FileController::class, 'updateGroup'])->where('group','[0-9]+')->where('path', '.*');
        // User Files
        Route::post('user/{user}/{path}',[FileController::class, 'uploadUser'])->where('path', '.*');
        Route::get('user/{user}/{path?}',[FileController::class, 'showUser'])->where('user','[0-9]+')->where('path', '.*');
        Route::put('user/{user}/{path}', [FileController::class, 'updateUser'])->where('user','[0-9]+')->where('path', '.*');

    });
    Route::prefix('user')->group(function(){
        Route::put('organization/{organization}', [UserController::class, 'changeOrganization']);

    });
    Route::prefix('users')->group(function(){
        Route::get('',  [UserController::class, 'index'])->name('users');
        Route::get('{user}', [UserController::class, 'show']);
        Route::post('', [UserController::class, 'store']);
        Route::post('search/{search?}', [UserController::class, 'search']);
    });
    Route::prefix('groups')->group(function(){
        Route::post('{group}/user/{user}', [GroupController::class, 'addUser']);
        Route::post('{group}/users', [GroupController::class, 'addUsers']);
        Route::delete('{group}/user/{user}', [GroupController::class, 'rmUser']);
        Route::delete('{group}/users', [GroupController::class, 'rmUsers']);
        Route::post('search/{search?}', [GroupController::class, 'search']);
        Route::post('/{group}/groups',   [GroupController::class, 'storeChild']);
    });
    Route::resource('groups', GroupController::class);
    Route::prefix('inventory')->group(function(){
        Route::get('/', [InventoryController::class, 'index'])->name('inventory');
    });
    Route::prefix('organizations')->group(function(){
        // Using group controller here since this is a generic group function
        Route::post(        '{organization}/users', [OrganizationController::class, 'addUsers']);
        Route::post(  '{organization}/user/{user}', [OrganizationController::class, 'addUser']);
        Route::delete(      '{organization}/users', [OrganizationController::class, 'rmUsers']);
        Route::delete('{organization}/user/{user}', [OrganizationController::class, 'rmUser']);
        Route::put(       '{organization}/feature', [OrganizationController::class, 'toggleFeature']);
        Route::post(            'search/{search?}', [OrganizationController::class, 'search']);
        Route::post(        '{organization}/group', [OrganizationController::class, 'storeGroup']);
    });
    Route::resource('organizations', OrganizationController::class);
    Route::resource('roles', RoleController::class);
});

Route::get('/help', [HelpController::class, 'help']);
Route::get('/help/{topic}', [HelpController::class, 'help']);
Route::get('/help/{topic}/{subtopic}', [HelpController::class, 'help']);
