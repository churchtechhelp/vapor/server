<?php

namespace Tests\Integration;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;
use App\Models\User;
use App\Models\Display;
use App\Models\DisplayRegistration;

class ClientTest extends TestCase {
    use RefreshDatabase;

    public function test_getCode(){
        // Get Code response
        $response = $this->postJson('/client/register',[]);
        
        // Test that there is a code
        $response->assertStatus(200)->assertJsonStructure([
            'code',
        ]);
        $this->assertIsString($response['code']);
    }

    public function testUnregistered(){
        $reg = DisplayRegistration::factory()->create();

        // Test that we are also unregistered
        $response = $this->postJson('/client/register',[
            'code' => $reg->code,
        ]);
        $response->assertStatus(200)->assertJson([
            'status' => 'unregistered',
        ]);
    }

    public function testGetRegistration(){
        $display = Display::factory()->has(DisplayRegistration::factory(),'registrations')->create();
        $reg = $display->registrations[0];
        $user = $display->joiner;
        $reg->assigned_at = now();
        $reg->joiner()->associate($user);
        $reg->save();

        // First time, we should receive the token code
        $response = $this->postJson('/client/register',[
            'code' => $reg->code,
        ]);
        $response->assertJsonStructure([
            'status',
            'token',
            'display',
        ]);
        $reg->refresh();
        $response->assertJson([
            'status' => 'registered',
            'token' => $reg->remember_token,
        ]);
        $token = $response['token'];
        $this->assertIsString($token);
        $this->assertGreaterThan(50,strlen($token));
    }
}
