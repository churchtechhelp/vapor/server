<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\User;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp(): void {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /**
     * Test that with no auth, we redirect.
     *
     * @return void
     */
    public function testDashboardRedirect(){
        $response = $this->actingAs($this->user)->get('/');
        // Redirect to the login, and then to the dashboard
        $response->assertRedirect('/login'); 
        $response = $this->actingAs($this->user)->get('/login');
        $response->assertRedirect('/dashboard'); 
    }

    public function testDashboardIndex(){
        $response = $this->actingAs($this->user)->get('/dashboard');
        $response->assertOK(); 
    }
}
