<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\User;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp(): void {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /**
     * Test that with no auth, we redirect.
     *
     * @return void
     */
    public function testShowIndex(){
        $response = $this->actingAs($this->user)->get('/shows');
        $response->assertOk();
    }
}
