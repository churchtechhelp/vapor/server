<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\User;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp(): void {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /**
     * Test that with no auth, we redirect.
     *
     * @return void
     */
    public function testNoAuthRedirectsToLogin(){
        $response = $this->get('/');

        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    public function testSeeLogin(){
        $response = $this->get('/login');
        $response->assertOk();
        $response->assertSee('Email');
        $response->assertSee('Password');
        $response->assertSee('Login');
        $response->assertViewIs('auth.login');
    }

    public function testCanLogin(){
        $response = $this->post('/login',[
            'email' => $this->user->email,
            'password' => 'password',
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/dashboard');
    }

    public function testLoginDisplaysValidationErrors(){
        // Send nothing so we will get an error bag
        $response = $this->post('/login',[]);
        $response->assertStatus(302); // redirect back
        $response->assertSessionHasErrors('email');
        $response->assertSessionHasErrors('password');
    }

    public function testSeeRegistration(){
        $response = $this->get('/register');
        $response->assertOk();
        $response->assertSee('Name');
        $response->assertSee('Password');
    }

    public function testRegistrationDisplaysValidationErrors(){
        // Send nothing so we will get an error bag
        $response = $this->post('/register',[]);
        $response->assertStatus(302); // redirect back
        $response->assertSessionHasErrors('name');
        $response->assertSessionHasErrors('email');
        $response->assertSessionHasErrors('password');
    }
}
