<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\User;

class FileTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp(): void {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /**
     * Test that with no auth, we redirect.
     *
     * @return void
     */
    public function testMyFiles(){
        $response = $this->actingAs($this->user)->get('/files');
        $response->assertOk();
    }

    public function testNonexistentUserFilesMissing(){
        $response = $this->actingAs($this->user)->get('/files/user/40');
        $response->assertStatus(404);
    }

    public function testOtherExistingUserFilesForbidden(){
        $user2 = User::factory()->create();
        $response = $this->actingAs($this->user)->get('/files/user/'.$user2->id);
        $response->assertStatus(403);
    }
}
