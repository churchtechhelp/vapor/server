<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\User;

class DisplayTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp(): void {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /**
     * Test that with no auth, we redirect.
     *
     * @return void
     */
    public function testDisplayIndex(){
        $response = $this->actingAs($this->user)->get('/displays');
        $response->assertOk();
    }
}
