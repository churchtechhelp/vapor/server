<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Organization;
use App\Models\Group;
use App\Models\Role;

class ManagementSearchTest extends TestCase
{
    use RefreshDatabase;

    public function testUserSearch()
    {
        $users = User::factory()->count(30)->create();
        $admin = $users[0];
        $testUser = $users[rand(1,29)];
        $shortName = substr($testUser->name,0,2);

        // Test to make sure normal queries are blocked
        $response = $this->actingAs($admin)->post('/users/search/' . urlencode($shortName));
        $response->assertStatus(403);
        
        // Make first user admin
        $adminRole = Role::create([
            'name' => 'administrator',
            'description' => 'example admin',
            'permissions' => ['admin'],
            'group_linked' => false,
        ]);
        $admin->roles()->attach($adminRole);
        

        $response = $this->actingAs($admin)->postJson('/users/search/' . urlencode($shortName));
        $response->assertOK();
        $this->assertGreaterThan(0,count($response->getData()));
    }

    // TODO: Organization/Group search test
}
