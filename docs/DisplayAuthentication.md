# Display Authentication

## Process Overview

1. Client runs for first time. The Code and UUID/Key are empty.
1. Client initiates registration with server, retrieving the code from the server.
1. The client occasionally checks for approval
1. A user registers the device, using the code as a reference
1. On next check, the client gets the key
1. Using the key, the client gets the current configuration
1. The client then opens a websocket to listen for changes
