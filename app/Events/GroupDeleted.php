<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

use App\Models\Group;
use App\Traits\Loggable;

class GroupDeleted
{
    use Dispatchable, SerializesModels, Loggable;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Group $group)
    {
        $this->user = \Auth::user();
        $this->object = $group;
        $this->class = 'Group';
    }
}
