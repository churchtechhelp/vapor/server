<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DomainNotCommon implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(empty($value)) return true;
        return !in_array($value,config('emailBlacklist'));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The domain is a common email. You must use a private domain.';
    }
}
