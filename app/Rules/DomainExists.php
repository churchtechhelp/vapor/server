<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DomainExists implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(empty($value)) return true;
        return checkdnsrr($value, 'A') || checkdnsrr($value, 'MX');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The domain given does not exist.';
    }
}
