<?php

namespace App\Http\Controllers;

use Laravel\Fortify\Http\Controllers\RegisteredUserController;
use Illuminate\Auth\Events\Registered;
use Laravel\Fortify\Contracts\RegisterResponse;
use Illuminate\Http\Request;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use ReCaptcha\ReCaptcha;

class RegistrationController extends RegisteredUserController {
    // Override store
    public function store(Request $request, CreatesNewUsers $creator): RegisterResponse
    {
        // Recaptcha, if the key is set
        if(env('RECAPTCHA_SERVER_SECRET',false)){
            $request->validate([
                'g-recaptcha-response' => 'required'
            ]);
            $recaptcha = new ReCaptcha(env('RECAPTCHA_SERVER_SECRET'));
            //$hostname = parse_url(env('APP_URL'), PHP_URL_HOST); // add ->setExpectedHostname($hostname) below
            $response = $recaptcha->setScoreThreshold(0.5)->verify($request->input('g-recaptcha-response'), $request->ip());
            if(!$response->isSuccess()){
                $errors =$response->getErrorCodes();
                return redirect()->back()->withErrors(['msg','recaptcha failed']);
            }
        }

        event(new Registered($user = $creator->create($request->all())));

        $this->guard->login($user);

        return app(RegisterResponse::class);
    }
}