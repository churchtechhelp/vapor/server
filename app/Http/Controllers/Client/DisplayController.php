<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Show;
use App\Models\Display;
use App\Models\DisplayRegistration;
use Storage;

class DisplayController extends Controller
{
    private $disk;

    public function __construct(){
        $this->disk = Storage::disk('user');
    }

    public function status(Display $user, Request $request){
        $request->validate([
            'status' => 'string|required',
        ]);

        // TODO: Validate that the state is good
        $user->status = $request->status;
        $user->save();
    }

    /**
     * 
     * returns Array
     * Name: name of main show
     * Main: main show ID
     * Shows: list of show shows by ID
     * Files: list of files to cache
     */
    public function config(){
        $ret = [];
        $display = \Auth::user();
        if(env('APP_DEBUG',false)){
            $ret['displayName'] = $display->name;
            $ret['displayId'] = $display->id;
        }
        $show = $display->show;
        if(!is_null($show)){
            $ret['name'] = $show->name;
            $ret['main'] = $show->id;
            $ret['files'] = [];
            $ret['shows'] = [];
            $ret['cache'] = [];
            $this->getShow($show,$ret['shows'],$ret['files'],$ret['cache']);
            $ret['files'] = $this->resolveModified($ret['files']);
        }
        return $ret;
        
    }

    public function file($path){
        // TODO: Check permissions before giving file away!
        return response()->file($this->disk->path($path));
    }

    private function getShow($show, &$shows, &$files, &$cache){
        // $show must be a integer or Show
        if(is_int($show)) $show = Show::find($show);
        if(!($show instanceof Show)) throw \Exception("Show not instance of show");
        // If we already got the show, we don't need to get it again (avoids loops)
        if(array_key_exists($show->id,$shows)) return;

        // Determine Shows
        $shows[$show->id]['type'] = ucfirst($show->config['type']);
        if($show->config['type'] == 'clock') {
                $shows[$show->id]['message'] = $show->config['clock']['content'];
        } elseif($show->config['type'] == 'single') {
            $shows[$show->id] = array_merge($shows[$show->id],$show->config['single']);
            if($show->config['single']['format'] == 'url') {
                // Need to cache the website
                // TODO: Need to search the entire page for external references
                $url = parse_url($show->config['single']['path']);
                $regex = "^" . $url['scheme'] . "://" . $url['host'];
                $regex = str_replace(".",'\.',$regex);
                //$regex = str_replace("/",'\/',$regex);
                $cache[] = $regex;
            } elseif(in_array($show->config['single']['format'],['application/vnd.apple.mpegurl','application/x-mpegURL'])) {
                // Stream - no cache, no files
            } else {
                // internal files
                $files[] = $show->config['single']['path'];
                if(array_key_exists('tapImage', $show->config['single'])) {
                    $files[] = $show->config['single']['tapImage'];
                }
            }
        } elseif($show->config['type'] == 'carousel'){
            $shows[$show->id]['shape'] = $show->config['carousel']['shape'];
        } elseif($show->config['type'] == 'grid'){
            // Grid adjust automatically, so all it needs is items
            // Future TODO: Grid Titles
        }
        if($show->config['type'] == 'carousel' || $show->config['type'] == 'grid') {
            $shows[$show->id]['items'] = $show->config['items'];
            foreach($show->config['items'] as $item) {
                if(array_key_exists('show_id',$item)){
                    // Is a show. Need to resolve
                    $this->getShow($item['show_id'],$shows,$files);
                } elseif(array_key_exists('path',$item)){
                    // Is a file. Need to add to cache list
                    $files[] = $item['path'];
                }
            }
        }
    }

    private function resolveModified($files) {
        $modFiles = [];
        foreach(array_unique($files) as $file){
            $modFiles[$file] = [
                'modified' => $this->disk->lastModified($file),
                'size' => $this->disk->size($file),
            ];
        }
        return $modFiles;
    }
}
