<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;

use App\Models\Display;
use App\Models\DisplayRegistration;

class AuthController extends Controller
{

    private $code = null;

    private $token = null;

    public function __construct(Request $request){
        // Get Code
        if($request->has('code')) $this->code = $request->code;
        elseif($request->hasHeader('Authorization_Code')) $this->code = $request->header('Authorization_Code');
        
        // Get Registration if it exists
        if($request->has('token')) $this->token = $request->token;
        elseif($request->hasHeader('Authorization_Token')) $this->token = $request->header('Authorization_Token');
    }

    public function register(Request $request){
        $ret = [];
        if(!is_null($this->code)){
            // Get Display object by code
            $displayreg = DisplayRegistration::where('code',$this->code)->first();
            if(is_null($displayreg)) return ['status'=>'invalid'];
            if(!is_null($this->token)) {
                if($this->token != $displayreg->remember_token) return abort(403);
                // Update Last seen time and IP
                $displayreg->heartbeat_at = now();
                $ip = $this->resolveIp($request);
                if(!is_null($ip)) $displayreg->last_seen_ip = $ip;
                $display = $displayreg->display;
                if(!is_null($display)){
                    $ret['status'] = 'registered';
                    $ret['display'] = $display->id;

                    if(!is_null($displayreg->display->show)) {
                        $ret['show_id'] = $displayreg->display->show->id;
                    }
                    // Cycle token every 90 days. Client should always accept a new token, otherwise the next auth will not work.
                    if($displayreg->remember_token_age->diffInDays(now()) > 90) {
                        $token = Str::random(60);
                        $displayreg->remember_token = $token;
                        $displayreg->remember_token_age = now();
                        $ret['previous_token'] = $request->token;
                        $ret['token'] = $token;
                    } 
                    
                } else {
                    // Registration removed or display deleted
                    $displayreg->remember_token = '';
                    $displayreg->assigned_at = null;
                    $displayreg->assigned_user_id = null;
                    // TODO: event for deleted display
                    $ret['status'] = 'deleted';
                }
                $displayreg->save();
            } else {
                if(!is_null($displayreg->assigned_at) && is_null($displayreg->remember_token)) {
                    // Assigned and has no token
                    // When assigned, we will send a random string
                    $token = Str::random(60);
                    $displayreg->remember_token = $token;
                    $displayreg->remember_token_age = now();
                    $displayid = $displayreg->display->id;
                    $displayreg->save();
                    $ret['status'] = 'registered';
                    $ret['token'] = $token;
                    $ret['display'] = $displayid;

                    // if not the production server, we need to tell the client where to go for the websocket
                    if(env('APP_ENV') == 'prod' && env('APP_URL') != 'https://vapor.churchtech.help'){
                        $ret['echo'] = [];
                        $ret['echo']['host'] = env('PUSHER_APP_HOST', 'localhost');
                        if(env('PUSHER_APP_PORT',443) != 443) $ret['echo']['port'] = env('PUSHER_APP_PORT');
                        $ret['echo']['secret'] = env('PUSHER_APP_SECRET');
                        $ret['echo']['key'] = env('PUSHER_APP_KEY');
                    }
                } elseif (!is_null($displayreg->assigned_at) && !is_null($displayreg->remember_token)) {
                    // assigned and has token
                    $ret['status'] = 'conflict';
                } else {
                    $ret['status'] = 'unregistered';
                }

            }
        } else {
            // No code. Need to make new unique code
            $reg = new DisplayRegistration;
            $reg->code = $this->generateUniqueCode(6);
            $reg->last_seen_ip = $this->resolveIp($request);
            $reg->heartbeat_at = now();
            $reg->save();
            $ret['code'] = $reg->code;
        }

        return $ret;
    }

    // Private Functions

    private function generateUniqueCode($length){
        for($try = 0; $try < 10; ++$try){
            $chars = "ABCDEFGHJKMNPQRSTUVWXYZ23456789@#$%&";
            $ret = "";
            $bytes = unpack('C*',random_bytes($length));
            foreach($bytes as $byte){
                $index = $byte % strlen($chars);
                $ret .= $chars[$index];
            }
            // Check if code exists
            if(DisplayRegistration::where('code',$ret)->count() == 0) return $ret;
        }
        return null;
    }

    private function resolveIp(Request $request){
        $headers = ['HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR'];
        foreach ($headers as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
        return $request->ip();
    }
}
