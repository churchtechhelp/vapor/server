<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\User;
use App\Models\Group;
use App\Models\Organization;
use App\Models\Role;

class UserController extends Controller
{
    public function __construct(){
        $this->authorizeResource(User::class, 'user');
    }

    public function index(Request $request){
        $user = $request->user();
        if(!is_null($user->organization)) return redirect('/organizations/' + $user->organization->id);
        $data = [];
        // Orgs
        if($user->hasPerm(['read-anything','read-orgs','group:read-anything'])){
            $data['orgcounts'] = [
                'total' => Organization::count(),
            ];
            $data['organizations'] = Organization::with('primaryUser')->orderBy('updated_at')->limit(10)->get();
        }
        // groups
        if($user->hasPerm(['read-anything','read-groups','group:read-anything','group:read-groups'])){
            $data['groupcounts'] = [
                'total' => Group::with('primaryUser')->whereNull('parent_id')->whereNull('organization_id')->count(),
            ];
            $data['groups'] = Group::with('primaryUser')->whereNull('parent_id')->whereNull('organization_id')->orderBy('updated_at')->limit(10)->get();
        }
        // users
        if($user->hasSitePerm(['read-anything','read-users'])){
            $data['usercounts'] = [
                'total' => User::count(),
                'active' => User::where('updated_at','>',\Carbon\Carbon::today()->subDays(30))->count(),
            ];
            $data['users'] = User::orderBy('updated_at')->limit(10)->get();
        }
        // Role Counts
        if($user->hasSitePerm(['read-anything','read-roles'])){
            $data['rolecounts'] = [
                'site' => Role::where('group_linked',false)->whereNull('group_id')->count(),
                'groups' => Role::whereNull('group_id')->where('group_linked',true)->count(),
                'group' => Role::whereNotNull('group_id')->count(),
            ];
        }
        return Inertia::render('Users/Index',$data);
    }

    public function show(Request $request, User $user){
        $user->loadMissing('groups');
        return Inertia::render('Users/User',[
            'targetUser' => $user,
        ]);
    }

    public function changeOrganization(Request $request, $organization){
        $user = $request->user();
        if($organization == 'null') {
            $user->current_organization_id = null;
        } else {
            $organization = $user->organizations()->find($organization);
            $user->current_organization_id = $organization->id;
        }
        $user->save();
        return redirect()->back();
    }

    public function login(){
        return Inertia::render('Login');
    }

    public function store(Request $request){
        $this->authorize('create');
        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
        User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => Hash::make($this->random_str(10)),
        ]);
        return redirect()->back();
    }

    public function search(Request $request, $search){
        //$this->authorize('viewAny');
        if($request->wantsJson()){
            if($request->has('parent')){
                [$type,$parentId] = explode('/',$request->parent,2);
                if($type == 'organization'){
                    $q = Organization::find($parentId)->users();
                } elseif($type == 'group'){
                    $q = Group::find($parentId)->users();
                } elseif($type == 'role'){
                    $q = Role::find($parentId)->users();
                } else {
                    throw new Exception("Error resolving parent type", 404);
                }
            } else {
                $q = User::select('name','email');
            }
            $q->where(function($sq) use ($search){
                if(env('DB_CONNECTION') == 'postgres'){
                    $sq->where('name','%',$search); // requires pg_trgm
                } else {
                    $sq->where('name','like',"%$search%");
                }
                $sq->orWhere('email','like',"%$search%")->limit(10);
            });
            return response()->json($q->get());
        } else {
            abort(403);
        }
    }

    private function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }
}
