<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class HelpController extends Controller
{
    public function help($topic = "",$subtopic = "") {
        // Main Page
        if($topic == "") return Inertia::render('Help/Index');
        // Validation
        $topic = str_replace(".","",$topic); // Don't want any dots in it already
        // Category Sub-Pages
        if($subtopic) {
            return Inertia::render("Help/".ucfirst($topic).'/'.ucfirst($subtopic),['topic'=>$topic]);
        } else {
            return Inertia::render("Help/" . ucfirst($topic));
        }
        //return Inertia::render('help/404',['view'=>"help.$topic"]);
    }
}
