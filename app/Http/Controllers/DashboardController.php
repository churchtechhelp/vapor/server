<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Models\Display;
use App\Models\Show;
use App\Models\Inventory;
use Auth;

class DashboardController extends Controller
{
    public function index() {

        $filesize = 0;
        $disk = \Storage::disk('user');
        foreach($disk->allFiles('/') as $file){
            $filesize += $disk->getSize($file);
        }

        return Inertia::render('Dashboard',[
            'shows' => Show::has('displays')->with('displays')->get(),
            'inventory' => Auth::user()->inventoryCheckouts,
            'filesize' => $filesize,
        ]);
    }
}
