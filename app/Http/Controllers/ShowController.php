<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Show;
use App\Models\User;
use App\Models\Display;

use App\Events\ClientDisplayUpdated;

class ShowController extends Controller
{
    public function index(Request $request){
        $user = $request->user();
        // Current user's shows
        $shows = Show::where('creator_user_id',$user->id)->get();
        // Shared/Group shows
        $shows->merge(Show::whereIn('group_id',$user->groupsWherePermission(['read-anything','read-shows'])));
        return Inertia::render('Shows/Index',[
            'shows' => $shows,
        ]);
    }

    public function store(Request $request){
        $validated = $request->validate([
            'name' => 'required|string',
            'description' => 'string',
        ]);
        $show = new Show;
        $show->name = $validated['name'];
        $show->creator()->associate(\Auth::user());
        $show->config = [
            'type' => 'blank',
        ];
        if($request->has('description')) $show->description = $validated['description'];
        else $show->description = $validated['name'];
        $show->save();
        return redirect('/shows/' . $show->id);
    }

    public function show(Show $show, Request $request) {
        $userId = $request->user()->id;
        // Find Paths
        $paths = [];
        // Single Show
        if(array_key_exists('single',$show->config)) $this->resolveShowPaths($show->config['single'], $userId, $paths);
        // Grid/Carousel Items
        if(array_key_exists('items',$show->config)) {
            foreach($show->config['items'] as $item){
                $this->resolveShowPaths($item, $userId, $paths);
            }
        }
        return Inertia::render('Shows/Show',[
            'show' => $show,
            'filenames' => $paths,
            'displays' => Display::all(),
            'deployed' => $show->displays,
        ]);
    }

    public function update(Show $show, Request $request) {
        $validated = $request->validate([
            'title' => 'string',
            'description' => 'string',
            'config' => 'array',
            'config.type' => 'required_with:config',
        ]);
        // TODO: Check if creator or in group
        if($request->has('title')) {
            $show->name = $validated['title'];
        }
        if($request->has('description')){
            $show->description = $validated['description'];
        }
        if($request->has('config')){
            $show->config = $validated['config'];
        }
        $show->save();
        foreach($show->displays as $display){
            event(new ClientDisplayUpdated($display));
        }
        return Inertia::render('Shows/Show',[
            'show' => $show,
        ]);
    }

    public function deploy(Show $show, Display $display){
        $display->show()->associate($show);
        $display->status = 'Awaiting Update';
        event(new ClientDisplayUpdated($display));
        $display->save();
        return redirect('/shows/' . $show->id);
    }

    public function deleteDeployment(Show $show, Display $display){
        $display->show()->dissociate($show);
        $display->status = 'Clearing';
        event(new ClientDisplayUpdated($display));
        $display->save();
        return redirect('/shows/' . $show->id);
    }

    private function resolveShowPaths($config, $currentUser, &$paths){
        if(array_key_exists('path',$config) && $config['format'] != 'stream') {
            $name = $this->resolvePrettyPath($config['path'],$currentUser);
            if(!is_null($name)) $paths[$config['path']] = $name;
        }
        if(array_key_exists('tapCoverName',$config)){
            $name = $this->resolvePrettyPath($config['tapCoverName'],$currentUser);
            if(!is_null($name)) $paths[$config['tapCoverName']] = $name;
        }
    }

    private function resolvePrettyPath($path, $currentUser){
        // If not a valid path, just return with nothing
        if(\Str::startsWith($path, 'http')) {
            $site = \file_get_contents($path);
            if(strlen($site) > 0){
                $site = trim(preg_replace('/\s+/', ' ', $site));
                preg_match("/\<title\>(.*)\<\/title\>/i",$site,$title);
                return $title[1];
            } else {
                return null;
            }
        }
        if(strlen($path) < 1) return null;

        // Now check the first part
        $splitpath = explode('/',$path);
        if($currentUser == $splitpath[0]) $name = "My ";
        else $name = User::find($splitpath[0])->name . "'s ";
        
        // Recreate string with nice username :)
        array_shift($splitpath);
        return $name . "Files/" . implode('/',$splitpath);
    }
}
