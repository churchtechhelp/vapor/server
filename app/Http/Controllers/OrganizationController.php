<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Rules\DomainExists;
use App\Rules\DomainNotCommon;

use App\Models\User;
use App\Models\Group;
use App\Models\Organization;
use App\Models\Role;

// Events
use App\Events\UserAddedToGroup;
use App\Events\UserRemovedFromGroup;

class OrganizationController extends Controller
{
    public function __construct(){
        $this->authorizeResource(Organization::class, 'organization');
    }

    public function show(Organization $organization){
        $organization->loadMissing('users','children');
        return Inertia::render('Organizations/Organization',[
            'organization' => $organization,
            'roles' => Role::where('group_linked',true)->orWhere('group_id',$organization->id)->get(),
        ]);
    }

    public function create(){
        return response(404);
    }

    public function store(Request $request){
        $data = $request->validate([
            'name' => 'required|unique:groups',
            'domain' => [new DomainNotCommon, new DomainExists],
        ]);
        $org = new Organization();
        $org->name = $data['name'];
        if($request->has('domain')) $org->domain = $data['domain'];
        $org->primary_user_id = $request->user()->id;
        $org->save();
        return redirect('/organizations/' . $org->id);
    }

    public function storeGroup(Request $request, Organization $organization){
        $data = $request->validate([
            'name' => 'required|unique:groups',
        ]);
        $group = new Group();
        $group->name = $data['name'];
        $group->primary_user_id = $request->user()->id;
        $group->organization()->associate($organization);
        $group->save();
        return redirect('/groups/' . $group->id);
    }

    public function destroy(Organization $organization, Request $request){
        $request->validate([
            'confirmed' => ['required','integer', function($attribute, $value, $fail){
                if(!empty($organization->children) && $value !== 2){
                    $fail('You must acknowledge that this will delete all groups and memberships.');
                }
            }],
        ]);
        Group::destroy($organization->getChildrenIds());
        return redirect()->back();
    }

    public function toggleFeature(Request $request, Organization $organization){
        $feature = $request->validate([
            'feature' => 'required|alpha',
        ])['feature'];
        $enabled = $organization->features;
        $index = array_search($feature, $enabled);
        if($index !== false) unset($enabled[$index]);
        else $enabled[] = $feature;
        $organization->features = $enabled;   
        $organization->save();
        return redirect()->back();
    }

    public function addUser(Organization $organization, User $user){
        $this->authorize('update', $organization);
        $organization->users()->attach($user);
        return redirect()->back();
    }
    public function rmUser(Organization $organization, User $user){
        $this->authorize('update', $organization);
        $organization->users()->detach($user);
        return redirect()->back();
    }
    public function addUsers(Organization $organization, Request $request){
        $this->authorize('update', $organization);
        $emails = explode(' ',str_replace([',',';'],' ',$request->emails));
        $users = User::wherein('email',$emails)->get();
        $organization->users()->attach($users);
        return redirect()->back();
    }
    public function rmUsers(Organization $organization, Request $request){
        $this->authorize('update', $organization);
        $emails = explode(' ',str_replace([',',';'],' ',$request->emails));
        $users = User::wherein('email',$emails)->get();
        $organization->users()->detach($users);
        return redirect()->back();
    }

    public function search(Request $request, $search){
        //$this->authorize('viewAny');
        if($request->wantsJson()){
            $q = Organization::select('name','domain');
            if(env('DB_CONNECTION') == 'postgres'){
                $q = $q->where('name','%',$search); // requires pg_trgm
            } else {
                $q = $q->where('name','like',"%$search%");
            }
            $q = $q->orWhere('domain','like',"%$search%")->limit(10);
            return response()->json($q->get());
        } else {
            abort(403);
        }
    }
}
