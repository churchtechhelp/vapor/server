<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Group;
use App\Models\Organization;
use App\Models\User;
use Auth;

class GroupController extends Controller
{
    public function index(){
        return redirect('/users');
    }

    public function show(Request $request, Group $group){
        $group->loadMissing('users','children','primaryUser');
        $breadcrumbs = [];
        foreach($group->parents() as $parent){
            $breadcrumbs[$parent->name] = $parent->link();
        } 
        if($parent->organization){
            $breadcrumbs[$parent->organization->name] = $parent->organization->link();
        }
        return Inertia::render('Groups/Group',[
            'group' => $group,
            'breadcrumbs' => array_reverse($breadcrumbs),
        ]);
    }

    public function store(Request $request){
        $this->authorize('create', [Display::class, $request]);
        $data = $request->validate([
            'name' => 'required|unique:groups',
        ]);
        $userid = Auth::user()->id;
        $group = new Group();
        $group->name = $data['name'];
        $group->primary_user_id = $userid;
        $group->save();
        return redirect('/groups/' . $newGroup->id);
    }

    public function storeChild(Request $request, Group $group){
        $this->authorize('create', [Display::class, $request]);
        $data = $request->validate([
            'name' => 'required|unique:groups',
        ]);
        $userid = Auth::user()->id;
        $newGroup = new Group();
        $newGroup->name = $data['name'];
        $newGroup->parent()->associate($group);
        if(!is_null($group->organization)) $newGroup->organization()->associate($group->organization);
        $newGroup->primary_user_id = $userid;
        $newGroup->save();
        return redirect('/groups/' . $newGroup->id);
    }

    public function destroy(Group $group, Request $request){
        $this->authorize('delete', $group);
        $request->validate([
            'confirmed' => ['required','integer', function($attribute, $value, $fail){
                if(!empty($group->children) && $value !== 2){
                    $fail('You must acknowledge that this will delete all groups and memberships.');
                }
            }],
        ]);
        Group::destroy($group->getChildrenIds());
        return redirect()->back();
    }

    public function addUser(Group $group, User $user){
        $this->authorize('update', $group);
        $organization->users()->attach($user);
        //return redirect()->back();
    }
    public function rmUser(Group $group, User $user){
        $this->authorize('update', $group);
        $organization->users()->detach($user);
        //return redirect()->back();
    }
    public function addUsers(Group $group, Request $request){
        $this->authorize('update', $group);
        $users = str_replace([',',';'],' ',$request->users);
        $users = User::wherein('email',explode(' ',$users))->get();
        $group->users()->attach($users);
        //return redirect()->back();
    }
    public function rmUsers(Group $group, Request $request){
        $this->authorize('update', $group);
        $users = str_replace([',',';'],' ',$request->users);
        $users = User::wherein('email',explode(' ',$users))->get();
        $group->users()->detach($users);
        //return redirect()->back();
    }

    public function search(Request $request, $search){
        //$this->authorize('viewAny');
        if($request->wantsJson()){
            $q = Group::select('name')->limit(10);
            if($request->has('parent')) {
                $q = $q->where('parent_id',$request->parent);
            } else {
                $q = $q->whereNull('parent_id');
            }
            // TODO: More permissions!
            if(env('DB_CONNECTION') == 'postgres'){
                $q = $q->where('name','%',$search); // requires pg_trgm
            } else {
                $q = $q->where('name','like',"%$search%");
            }
            return response()->json($q->get());
        } else {
            abort(403);
        }
    }
}
