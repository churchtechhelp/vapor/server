<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Models\Role;

class RoleController extends Controller
{
    public function __construct(){
        $this->authorizeResource(Role::class, 'role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Inertia::render('Roles',[
            'roles' => Role::all(),
            'permissions' => config('permissions'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'         => 'required|string', 
            'description'  => 'string',
            'permissions'  => 'required|string',
            'group'        => 'boolean',
        ]);
        $role = new Role;
        $role->name = $validated['name'];
        $role->description = $validated['description'];
        $role->permissions = explode(' ',$validated['permissions']);
        $role->group_linked = $validated['group'] == 1 || $validated['group'] === true || $validated['group'] === "true";
        $role->save();
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $validated = $request->validate([
            'name'         => 'required|string', 
            'description'  => 'string',
            'permissions'  => 'required|string',
            'group'        => 'boolean',
        ]);
        $role->name = $validated['name'];
        $role->description = $validated['description'];
        $role->permissions = explode(' ',$validated['permissions']);
        $role->group_linked = $validated['group'] == 1 || $validated['group'] === true || $validated['group'] === "true";
        $role->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->users()->detach();
        $role->delete();
        return redirect()->back();
    }
}
