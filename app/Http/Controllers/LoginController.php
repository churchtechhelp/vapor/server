<?php

namespace App\Http\Controllers;

use ReCaptcha\ReCaptcha;
use Laravel\Fortify\Contracts\LoginResponse;
use Laravel\Fortify\Http\Requests\LoginRequest;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;

class LoginController extends AuthenticatedSessionController {
    /**
     * Attempt to authenticate a new session.
     *
     * @param  \Laravel\Fortify\Http\Requests\LoginRequest  $request
     * @return mixed
     */
    public function store(LoginRequest $request)
    {
        // Recaptcha, if the key is set
        if(env('RECAPTCHA_SERVER_SECRET',false)){
            $request->validate([
                'g-recaptcha-response' => 'required'
            ]);
            $recaptcha = new ReCaptcha(env('RECAPTCHA_SERVER_SECRET'));
            //$hostname = parse_url(env('APP_URL'), PHP_URL_HOST); // add ->setExpectedHostname($hostname) below
            $response = $recaptcha->setScoreThreshold(0.5)->verify($request->input('g-recaptcha-response'), $request->ip());
            if(!$response->isSuccess()){
                $errors =$response->getErrorCodes();
                return redirect()->back()->withErrors(['msg','recaptcha failed']);
            }
        }

        return $this->loginPipeline($request)->then(function ($request) {
            return app(LoginResponse::class);
        });
    }
}