<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\File;
use App\Models\User;
use App\Models\Group;
use Storage;
use Auth;
use Gate;
use Symfony\Component\Mime\MimeTypes;

class FileController extends Controller
{
    public function index(Request $request){
        return $this->showUser($request->user(), $request, '');
    }

    // root upload for user
    public function store(Request $request) {
        return $this->uploadUser($request, $request->user());
    }

    public function uploadUser(Request $request, User $user, $path = ''){
        if(Gate::denies('edit-file', [$user, $path])) abort(403);
        $path = $user->id . '/' . $path;
        return $this->upload($request,'user',$path);
    }

    public function uploadGroup(Request $request, Group $group, $path = ''){
        $path = $group->id . '/' . $path;
        return $this->upload($request,'group',$path);
    }
    
    private function upload(Request $request, $type, $path) {
        if(Gate::denies('view-file',[ $request->user(), $path])) abort(403);
        // TODO: events
        $request->validate([
            'file' => 'required|file|mimes:tar,zip,bmp,gif,jpeg,png,svg,tiff,webp,ico,mp4,ogv,html,pdf,txt',
        ]);
        // TODO: Check permissions for request path
        $extension = $request->file('file')->extension();
        $filePath = $request->file('file')->getRealPath();
        // TODO: Gzip+tar: Use gzopen, output to temp file, and then update $path and $extension
        if($extension == 'gz') {
            $gz = gzopen($filePath,'rb');
            $filePath = substr($filePath,0,-3); // remove the .gz
            $gzout = fopen($filePath, 'wb');
            // Need to update the extenison and mime
            $mimeTypes = new MimeTypes();
            $mime = $mimeTypes->guessMimeType($filePath);
            $extension = explode('.',$filePath)[-1];
        }
        
        // TODO: Check if file exists
        // Storage::disk('user')->exists(filename)
        if($extension == 'zip' || $extension == 'tar') {
            $name = substr($request->file('file')->getClientOriginalName(),0,-4);
            $zip = new \PharData($filePath);
            $zip->extractTo(\Storage::disk($type)->path($path . '/' . $name));
        } else {
            $request->file('file')->storeAs($path,$request->file('file')->getClientOriginalName(), $type);
        }
        
        return redirect('/files/'.$type.'/'.$path);
    }

    public function showUser(User $user, Request $request, $path = ''){
        if(Gate::denies('view-file',[ $user, $path])) abort(403);
        $path = $user->id . '/' . $path;
        return $this->show('user', $user->name, $request, $path);
    }

    public function showGroup(Group $group, Request $request, $path = ''){
        if(Gate::denies('view-file-group',[ $group, $path])) abort(403);
        $path = $group->id . '/' . $path;
        return $this->show('group', $group->name, $request, $path);
    }

    public function show($type, $name, Request $request, $path){
        $disk = Storage::disk($type);
        $splitPath = explode('/',$path);
        $id = $splitPath[0];
        $fullPath = $disk->path($path);
        if(Gate::allows('view-file', [$request->user(), $path])) {
            if(is_dir($fullPath)) {
                $renderData = [
                    'files' => $this->getFolder($type,$path,$request),
                    'path' => $path,
                    'username' => $name,
                ];
                if($request->has('list')){ // the list parameter disables inertia view
                    return $renderData;
                } 
                return Inertia::render('Files/Folder', $renderData);
            } elseif(!$disk->exists($path)) {
                if(count(array_filter($splitPath)) == 1){
                    if (Gate::allows('edit-file', [$request->user(), $path])) {
                        // User folder does not exist. Need to create (assuming user exists since $user resolves)
                        $disk->makeDirectory($path);
                        // And then return it, with an empty file structure
                        return Inertia::render('Files/Folder',[
                            'files' => [],
                            'path' => $path,
                            'username' => $name,
                        ]);
                    }
                }
                return Inertia::render('Files/Missing',[
                    'path' => $path,
                    'username' => $name,
                ]);
            } else {
                if($request->has('download')) {
                    return response()->download($fullPath);
                }
                if($request->has('raw')) {
                    // Need to override SVG so it renders on the browser
                    if($disk->mimeType($path) == 'image/svg') return response()->file($fullPath,[
                        'Content-Type' => 'image/svg+xml',
                    ]);
                    return response()->file($fullPath);
                }
                // Otherwise, return a view
                $renderData = [
                    'name' => basename($fullPath),
                    'updated_at' => $disk->lastModified($path),
                    'format' => $disk->mimeType($path),
                    'size' => $disk->size($path),
                    'url' => $disk->url($path),
                    'path' => $path,
                    'username' => $name,
                ];
                if(\Str::startsWith($renderData['format'],'text')){
                    // See MIME's: https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types
                    $renderData['file'] = $disk->get($path);
                    $splitFormat = explode('/',$renderData['format']);
                    if(\Str::endsWith($path, '.vue')) {
                        $renderData['language'] = 'vue';
                    } elseif(\Str::endsWith($path, '.php')) {
                        $renderData['language'] = 'php';
                    } else {
                        $renderData['language'] = $splitFormat[1];
                        // remove x-
                        if(\Str::startsWith($renderData['language'],'x-')) $renderData['language'] = substr($renderData['language'],2);
                    }
                }
                
                if($request->has('list')){
                    abort(500, "Cannot list a file");
                    return $renderData;
                } 
                return Inertia::render('Files/File',$renderData);
            }
        } else {
            abort(403);
        }
    }

    public function updateUser(User $user, Request $request, $path){
        $path = $user->id . '/' . $path;
        return $this->update(Storage::disk('user'), $request, $path);
    }
    
    public function updateGroup(Group $group, Request $request, $path){
        $path = $group->id . '/' . $path;
        return $this->update(Storage::disk('group'), $request, $path);
    }

    public function update($disk, Request $request, $path){
        if(Gate::denies('view-file', [$request->user(), $path])) abort(403);
        $request->validate([
            'content' => 'required|string',
        ]);
        $disk->put($path, $request->content);
        return redirect()->back();
    }

    private function getFolder($type,$dir,$request){
        $disk = Storage::disk($type);
        $path = $disk->path($dir);
        $splitpath = explode('/',$dir);
        if(!$disk->exists($dir)) return [];
        $directories = $disk->directories($dir);
        $straightFiles = $disk->files($dir);
        if($type == 'user' && $splitpath[0] == $request->user()->id) $prettyPath = "My Files/";
        //else $prettyPath = $user->name . "'s Files/";
        else $prettyPath = "X-Files/";
        $files = [];
        foreach($straightFiles as $name){
            $format = $disk->mimeType($name);
            if($request->has('filter')){
                // If filter exists on request, file types must contain one of the strings separated by a comma
                $desiredTypes = explode(',',$request->filter);
                if(!\Str::contains($format,$desiredTypes)) continue;
            }
            $files[] = [
                'name' => substr($name,strlen($dir)),
                'updated_at' => $disk->lastModified($name),
                'format' => $format,
                'size' => $disk->size($name),
                'url' => $disk->url($name),
                'path' => $type . '/' . $name,
                //'ospath' => $disk->path($name),
                'prettypath' => $prettyPath . substr($name,strpos($name,'/')+1), // remove the user part
            ];
        }
        foreach($directories as $name){
            $files[] = [
                'name' => substr($name,strlen($dir)),
                'updated_at' => $disk->lastModified($name),
                'format' => 'folder',
                'size' => 0,
                'url' => $disk->url($name),
                'path' => $disk->path($name),
            ];
        }
        return $files;
    }
}
