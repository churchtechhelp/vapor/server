<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Models\Display;
use App\Models\DisplayRegistration;

use App\Events\ClientDisplayUpdated;

class DisplayController extends Controller
{
    public function __construct(){
        $this->authorizeResource(Display::class, 'display');
    }

    public function index(Request $request){
        $user = $request->user();
        if($user->hasSitePerm(['read-anything','read-displays'])){
            // Admin can see ALL displays
            // TODO: Maybe make an option to see all?
            $displays = Display::with('joiner')->with('registrations')->get();
        } elseif(is_null($user->currentOrganization)) {
            // Personal Displays
            $displays = Display::with('joiner')->with('registrations')->where('join_user_id',$user->id)->get();
        } else { // get current organization's Displays (this is an admin page...)
            $org_groups = $user->currentOrganization->allChildren()->pluck("id")->all();
            $displays = Display::with('joiner')->with('registrations')->whereIn('group_id',$org_groups)->get();
        }
        
        return Inertia::render('Displays/Index',[
            'displays' => $displays,
        ]);
    }

    public function show(Display $display){
        $display->loadMissing('registrations');
        return Inertia::render('Displays/Display',[
            'display' => $display,
        ]);
    }

    public function approve(Display $display, DisplayRegistration $displayReg){
        $this->authorize('register', $display);
        // If display is already set, than we cannot run this function
        if(!is_null($displayReg->display)) abort(500);

        // Need to set display, approved time, and approval user
        $displayReg->assigned_at = now();
        $displayReg->joiner()->associate(\Auth::user());
        $displayReg->display()->associate($display);
        $displayReg->save();
    }

    public function create(Request $request){
        $request->validate([
            'name' => 'required|string',
            'code' => 'nullable|string|size:6',
        ]);
        $display = new Display;
        $display->name = $request->name;
        $display->joiner()->associate(\Auth::user());
        
        if($request->has('code')) {
            $reg = DisplayRegistration::where('code',$request->code)->firstOrFail();
            // We are registering a display at the same time.
            $display->save();
            $this->approve($display, $reg);
        } else {
            $display->save();
        }
        return redirect('/displays');
    }

    public function delete(Display $display){
        // Disassociate all registrations
        foreach($display->registrations as $reg){
            $reg->display()->dissociate();
            $reg->save();
        }
        event(new ClientDisplayUpdated($display));
        // Delete
        $display->delete();
        return redirect('/displays'); 
    }

    public function register(Display $display, Request $request){
        $data = $request->validate([
            'code' => 'required|string|size:6',
        ]);
        $reg = DisplayRegistration::where('code',$data['code'])->firstOrFail();
        $this->approve($display, $reg);
        return redirect('/displays/' . $display->id);
    }

    public function unregister(Display $display, DisplayRegistration $registration){
        event(new ClientDisplayUpdated($display));
        $registration->display()->disassociate();
        $registration->save();
        return redirect('/displays/' . $display->id);
    }
}
