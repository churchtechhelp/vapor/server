<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Inventory;

class InventoryController extends Controller
{
    public function __construct(){
        $this->authorizeResource(Role::class, 'role');
    }
    
    public function index(){
        return Inertia::render('Inventory',[
            'catalog' => Inventory::all(),
        ]);
    }
}
