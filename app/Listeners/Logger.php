<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Event as EventLog;

class Logger
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $e = new EventLog();
        $e->title = get_class($event);
        $e->user_id = $event->user->id;
        $e->target_type = $event->class;
        $e->target_id = $event->id;
        $e->data = ['backup' => $event->object->toArray() ];
        $e->save();
    }

    public function subscribe(){
        return [
            App\Events\GroupDeleted::class => [
                Logger::class, 'handle'
            ],
        ];
    }
}
