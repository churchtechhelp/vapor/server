<?php

namespace App\Policies;

use App\Models\Display;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DisplayPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        if($user->hasSitePerm(['read-anything','read-displays'])) return true;
        if($user->ownsFeature('displayManagement') && !$user->hasPermission(['group:read-anything','group:read-displays'])) return false;
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Display  $display
     * @return mixed
     */
    public function view(User $user, Display $display)
    {
        // Is owner
        if($user->id == $display->joiner->id) return true;

        // Is Site admin
        if($user->hasSitePerm(['read-anything','read-displays'])) return true;

        // Is display group -> group member
        if(!is_null($display->group) && $display->group->group->has($user)){
            return true;
        }

        // Is inventory checked out
        if(!is_null($display->inventory) && $display->inventory->checkouts()->where('user_id',$user->id)->count() > 0){
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if($user->ownsFeature('displayManagement') && !$user->hasPerm(['group:write-anything','group:write-display','group:join-display'])) return false;
        return true;
    }

    public function register(User $user, Display $display){
        if($user->id == $display->joiner->id) return true;
        if($user->hasPerm(['write-anything','write-displays','group:write-anything','group:write-display'])) return true;
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Display  $display
     * @return mixed
     */
    public function update(User $user, Display $display)
    {
        // Is owner
        if($user->id == $display->joiner->id) return true;
        if($user->hasSitePerm(['write-anything','write-displays'])) return true;
        if($user->ownsFeature('displayManagement') && $user->hasPerm(['group:write-anything','group:write-display'])) return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Display  $display
     * @return mixed
     */
    public function delete(User $user, Display $display)
    {
        // Is owner
        if($user->id == $display->user()->id) return true;
        if($user->hasSitePerm(['write-anything','write-displays'])) return true;
        if($user->ownsFeature('displayManagement') && $user->hasPerm(['group:write-anything','group:write-display'])) return true;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Display  $display
     * @return mixed
     */
    public function restore(User $user, Display $display)
    {
        // Is owner
        if($user->id == $display->user()->id) return true;
        if($user->hasSitePerm(['write-anything','write-displays'])) return true;
        if($user->ownsFeature('displayManagement') && $user->hasPerm(['group:write-anything','group:write-display'])) return true;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Display  $display
     * @return mixed
     */
    public function forceDelete(User $user, Display $display)
    {
        // Is owner
        if($user->id == $display->user()->id) return true;
        if($user->hasSitePerm(['write-anything','write-displays'])) return true;
        if($user->ownsFeature('displayManagement') && $user->hasPerm(['group:write-anything','group:write-display'])) return true;
    }
}
