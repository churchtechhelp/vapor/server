<?php

namespace App\Policies;

// TODO: Events to log file access
use App\Models\User;
use App\Models\Group;

class FilePolicy {
    public function view(User $currentUser, User $targetUser, $path){
        if($targetUser->id == $currentUser->id) return true; // User can access all their own files
        if($currentUser->hasSitePerm(['read-anything','read-files'])) return true;
        return false;
    }
    public function viewGroup(User $currentUser, Group $group, $path){
        //if($targetUser->id == $currentUser->id) return true; // User can access all their own files
        if($currentUser->hasSitePerm(['read-anything','read-files'])) return true;
        // TODO: Group Permissions. How to check if target user is in group?
        //return false;
        return $currentUser->inGroup($group->id);
    }

    public function edit(User $currentUser, User $targetUser, $path){
        if($targetUser->id == $currentUser->id) return true; // User can access all their own files
        if($currentUser->hasSitePerm(['write-anything','write-files'])) return true;
        return false;
    }

    public function editGroup(User $currentUser, Group $group, $path){
        //if($targetUser->id == $currentUser->id) return true; // User can access all their own files
        if($currentUser->hasSitePerm(['write-anything','write-files'])) return true;
        // TODO: Group Permissions. How to check if target user is in group?
        //return false;
        return $currentUser->inGroup($group->id);
    }

    public function list(User $currentUser, $targetUser, $path){
        if($targetUser->id == $currentUser->id) return true; // User can access all their own files
        if($currentUser->hasSitePerm(['read-anything','read-files'])) return true;

        return false;
    }
}