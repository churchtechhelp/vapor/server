<?php

namespace App\Policies;

use App\Models\Group;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasSitePerm(['read-anything','read-groups']);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Group  $group
     * @return mixed
     */
    public function view(User $user, Group $group)
    {
        return $user->hasSitePerm(['read-anything','read-groups']) || $user->hasGroupPerm($group,['read-anything','read-users']);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param Request $request
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user, Request $request)
    {
        if($user->hasSitePerm(['write-anything','write-groups'])) return true;
        if($request->has('parent')){
            $group = Group::find($request->parent);
            if(is_null($group)) return false;
            return $user->hasGroupPerm($group,['write-anything','write-users']);
        } 
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Group  $group
     * @return mixed
     */
    public function update(User $user, Group $group)
    {
        if($user->hasSitePerm(['write-anything','write-groups'])) return true;
        return $user->hasParentGroupPerm($group,['write-anything','write-users']);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Group  $group
     * @return mixed
     */
    public function delete(User $user, Group $group)
    {
        if($user->hasSitePerm(['write-anything','write-groups'])) return true;
        return $user->hasParentGroupPerm($group,['write-anything','write-users']);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Group  $group
     * @return mixed
     */
    public function restore(User $user, Group $group)
    {
        if($user->hasSitePerm(['write-anything','write-groups'])) return true;
        return $user->hasParentGroupPerm($group,['write-anything','write-users']);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Group  $group
     * @return mixed
     */
    public function forceDelete(User $user, Group $group)
    {
        return $user->hasSitePerm(['write-anything','write-groups']);
    }
}
