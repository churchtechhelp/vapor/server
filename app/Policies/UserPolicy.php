<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // note that this is the page that currently shows orgs, groups, and users
        return $user->hasSitePerm(['read-anything','read-users','read-groups','read-orgs','group:read-anything','group:read-users','group:read-groups']);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        if($user->id == $model->id) return true;
        return $user->hasPerm(['read-anything','read-users']);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if($user->hasSitePerm(['write-anything','write-users'])) return true;
        if(is_null($user->currentOrganization)) return false;
        return $user->hasPerm(['group:write-anything','group:write-users']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        if($user->id == $model->id) return true;
        return $user->hasPerm(['write-anything','write-users','group:write-anything','group:write-users']);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        if($user->id == $model->id) return true;
        return $user->hasPerm(['write-anything','write-users','group:write-anything','group:write-users']);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        return $user->hasPerm(['write-anything','write-users','group:write-anything','group:write-users']);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        return $user->hasPerm(['write-anything','write-users','group:write-anything','group:write-users']);
    }
}
