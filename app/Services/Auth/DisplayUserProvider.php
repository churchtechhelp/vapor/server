<?php

namespace App\Services\Auth;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Auth\AuthenticationException;
use App\Models\Display;
use App\Models\DisplayRegistration;

class DisplayUserProvider implements UserProvider {
    // Remember $user == the display
    public function retrieveById($identifier){
        return Display::find($identifier);
    }
    public function retrieveByToken($identifier, $token){
        $reg = DisplayRegistration::where('code',$identifier)->where('remember_token',$token)->whereNotNull('assigned_at')->first();
        if(is_null($reg)) throw new AuthenticationException;
        return $reg->display;
    }
    public function updateRememberToken(Authenticatable $user, $token){
        // The "token" *never* gets updated from here
    }
    public function retrieveByCredentials(array $credentials){
        return DisplayRegistration::where('code',$credentials[0])->where('remember_token',$credentials[1])->whereNotNull('assigned_at')->first()->display;
    }
    public function validateCredentials(Authenticatable $user, array $credentials){
        // $user is the display. Need to check for DisplayRegistrations
        return $user->registrations()->where('code',$credentials[0])->where('remember_token',$credentials[1])->whereNotNull('assigned_at')->count() == 1;
    }
}
