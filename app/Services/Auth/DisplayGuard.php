<?php

namespace App\Services\Auth;

use BadMethodCallException;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DisplayGuard implements Guard {
    // We are dealing with a UserProvider, so don't get confused with display == user
    use GuardHelpers;

    /**
     * Create a new authentication guard.
     *
     * @param  callable  $callback
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Contracts\Auth\UserProvider|null  $provider
     * @return void
     */
    public function __construct(Request $request, UserProvider $provider = null)
    {
        $this->request = $request;
        $this->provider = $provider;
    }

    /**
     * Get the currently authenticated user.
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user(){
        if (!is_null($this->user)) {
            return $this->user;
        }

        // We don't have the user. We need to find the registration and get the user
        if($this->request->hasHeader('Authorization_Code')) {
            return $this->user = $this->provider->retrieveByToken($this->request->header('Authorization_Code'),$this->request->header('Authorization_Token'));
        }
        if($this->request->has('code')) {
            return $this->user = $this->provider->retrieveByToken($this->request->code,$this->request->token);
        }
    }

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return int|string|null
     */
    public function id(){
        return $this->user->id;
    }

    /**
     * Validate a user's credentials.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function validate(array $credentials = []){
        $user = $this->provider->retrieveByCredentials($credentials);
        return !is_null($user) && $this->provider->validateCredentials($user, $credentials);
    }
}
