<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\Role;
use App\Models\User;

class FirstTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'first-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'First-time Setup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(Role::count() > 0 || User::count() > 0) {
            echo("Initial Seeder can only be run in an empty database.");
            return 1;
        }

        echo("\nWelcome to the Vapor DGS first-time setup!\n\n");
        $handle = fopen ("php://stdin","r");
        echo("Please type in email for administrator user: ");
        $email = trim(fgets($handle));
        $password = $this->getPassword();
        echo("\nThank you. Now setting up DB for first time.\n");

        $adminRole = Role::create([
            'name' => 'Site Admin',
            'description' => 'Access to everything',
            'permissions' => ['admin'],
        ]);
        Role::create([
            'name' => 'Site Auditor',
            'description' => 'Read Access to everything',
            'permissions' => ['read-anything'],
        ]);
        Role::create([
            'name' => 'Group Administrator',
            'description' => 'Administrates a Group. Has full access to all group resources',
            'permissions' => ['admin'],
            'group_linked' => true,
        ]);
        Role::create([
            'name' => 'Group User Administrator',
            'description' => 'Controls users in the group',
            'permissions' => ['write-users','read-users'],
            'group_linked' => true,
        ]);
        User::create([
            'name' => 'Administrator',
            'email' => $email,
            'email_verified_at' => now(),
            'password' => Hash::make($password), // password
            'remember_token' => Str::random(10),
        ])->roles()->attach($adminRole);
        echo("Complete!\nYou now should be able to login using the email and username provided.\n");
        return 0;
    }

    private function getPassword(){
        if (preg_match('/^win/i', PHP_OS)) {
            echo("\nPlease type in the password for the administrator user: ");
            $password = trim(shell_exec('powershell.exe [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR((Read-Host -AsSecureString)))'));
        } else {
            echo("\nPlease type in the password for the administrator user (No echo): ");
            system('stty -echo');
            $handle = fopen ("php://stdin","r");
            $password = trim(fgets($handle));
            system('stty echo');
        }
        return $password;
    }
}
