<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\DisplayRegistration;

class CleanRegistrations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registrations:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes registrations older than a day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DisplayRegistration::whereNull('assigned_at')->whereDate('updated_at','<',now()->subDay()->toDateString())->delete();
        return 0;
    }
}
