<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;

class Display extends Model implements Authenticatable
{
    use HasFactory;
    use Notifiable;

    protected $appends = ['heartbeat'];

    public function events(){
        return $this->morphMany('App\Models\Event', 'target');
    }

    public function show() {
        return $this->belongsTo('App\Models\Show');
    }

    public function inventory(){
        return $this->belongsTo('App\Models\Inventory');
    }

    public function joiner(){
        return $this->belongsTo('App\Models\User', 'join_user_id');
    }

    public function displayGroup(){
        return $this->belongsTo('App\Models\DisplayGroup');
    }

    public function group(){
        return $this->belongsTo('App\Models\Group');
    }

    public function getHeartbeatAttribute(){
        $latestRegistration = $this->registrations()->orderBy('heartbeat_at')->first();
        if(is_null($latestRegistration)) return null;
        return $latestRegistration->heartbeat_at;
    }

    // Autentication ////////////////////////////////////////////////
    /**
     * The access token the user is using for the current request.
     *
     * @var App\Models\DisplayRegistration
     */
    protected $registration;

    /**
     * Get the registrations that belong to display
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function registrations(){
        return $this->hasMany('App\Models\DisplayRegistration');
    }

    /**
     * Set the current access token for the user.
     *
     * @param  App\Models\DisplayRegistration $registration
     * @return $this
     */
    public function withRegistration(DisplayRegistration $registration)
    {
        $this->registration = $registration;

        return $this;
    }

    // Authenticable //////////////////////////////////////
    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName(){
        return 'id';
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier(){
        return $this->id;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword(){
        return $this->remember_token;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken(){
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value){
        $this->remember_token = $value;
        $this->save();
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName(){
        return 'remember_token';
    }
}
