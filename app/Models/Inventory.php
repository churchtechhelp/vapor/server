<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;

    public function events(){
        return $this->morphMany('App\Models\Event', 'target');
    }

    public function type(){
        return $this->hasOneThrough('App\Models\InventoryModel', 'App\Models\InventoryType');
    }

    public function manufacturer(){
        return $this->hasOneThrough('App\Models\InventoryModel', 'App\Models\InventoryManufacturer');
    }

    public function model(){
        return $this->belongsTo('App\Models\InventoryModel');
    }

    public function group(){
        return $this->belongsTo('App\Models\Group');
    }

    public function getOrganizationAttribute(){
        return $this->group->org();
    }

    public function checkouts(){
        return $this->hasMany('App\Models\InventoryCheckout');
    }
}
