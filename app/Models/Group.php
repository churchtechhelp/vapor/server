<?php

namespace App\Models;

use App\Events\GroupDeleted;

class Group extends GroupBase
{
    public static function booted(){
        static::addGlobalScope(function ($query) {
            $query->where('is_organization', false);
        });
    }

    public function parent(){
        return $this->belongsTo('App\Models\Group', 'parent_id');
    }

    public function parents(){
        return Group::hydrate(\DB::select("WITH traverse AS (
            SELECT *, 0 depth FROM groups
            WHERE id = ?
            UNION ALL
            SELECT groups.*, traverse.depth + 1 FROM groups
            INNER JOIN traverse
            ON traverse.parent_id = groups.id
        )
        SELECT * from traverse",[$this->id]));
    }

    public function organization(){
        return $this->belongsTo('App\Models\Organization');
    }

    // EVENTS
    
}
