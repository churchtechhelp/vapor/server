<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DisplayGroup extends Model
{
    use HasFactory;

    public function events(){
        return $this->morphMany('App\Models\Event', 'target');
    }

    public function group(){
        return $this->belongsTo('App\Models\Group');
    }

    public function displays(){
        return $this->hasMany('App\Models\Display');
    }
}
