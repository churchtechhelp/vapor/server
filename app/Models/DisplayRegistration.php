<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DisplayRegistration extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'remember_token',
    ];

    protected $hidden = [
         'remember_token',
    ];

    protected $dates = [
        'remember_token_age',
        'heartbeat_at'
    ];

    public function events(){
        return $this->morphMany('App\Models\Event', 'target');
    }

    public function display(){
        return $this->belongsTo('App\Models\Display');
    }

    public function joiner(){
        return $this->belongsTo('App\Models\User', 'assigned_user_id');
    }
}
