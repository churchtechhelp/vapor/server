<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Contracts\Auth\CanResetPassword;

use App\Models\Group;

class User extends Authenticatable implements MustVerifyEmail {
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
        'permissions', // base permissions, not group permissions. Need context for that!
        'features',
    ];

    protected $with = [
        'organizations',
    ];

    // TODO: Role ID

    public function events(){
        return $this->morphMany('App\Models\Event', 'target');
    }

    public function groups(){
        return $this->belongsToMany('App\Models\Group');
    }

    public function createdShows(){
        return $this->hasMany('App\Models\Show', 'creator_user_id');
    }

    public function shows(){
        return $this->belongsToMany('App\Models\Show');
    }

    public function archivedShows(){
        return $this->hasMany('App\Models\Show', 'archived_user_id');
    }

    public function files(){
        return $this->hasMany('App\Models\File', 'owner_id');
    }

    public function roles() {
        return $this->belongsToMany('App\Models\Role')->withPivot('group_id');
    }

    public function getPermissionsAttribute(){
        $permissions = [];
        foreach($this->roles as $role){
            if(!is_null($role->pivot->group_id)){
                $permissions = array_merge($permissions,array_map(function($el){
                    return 'group:' . $role->pivot->group_id . ':' . $el;
                },$role->allperms()));
            } else {
                $permissions = array_merge($permissions,$role->allperms());
            }
        }
        return $permissions;
    }

    public function getSitePermissionsAttribute(){
        $permissions = [];
        foreach($this->roles as $role){
            if(!is_null($role->pivot->group_id)) continue;
            $permissions = array_merge($permissions,$role->allperms());
        }
        return $permissions;
    }

    // Features that we should show, relative to current_organization_id
    public function getFeaturesAttribute(){
        if(is_null($this->current_organization_id)) return [];
        return $this->currentOrganization->features;
    }

    public function ownsFeature($feature){
        return in_array($feature,$this->features);
    }

    public function joinedDisplays() {
        return $this->hasMany('App\Models\Display','join_user_id');
    }

    public function organizations() {
        return $this->belongsToMany('App\Models\Organization','group_user','user_id','group_id');
    }

    public function currentOrganization(){
        return $this->belongsTo('App\Models\Organization','current_organization_id');
    }

    public function inventoryCheckouts() {
        return $this->hasMany('App\Models\InventoryCheckout');
    }

    public function resolveGroups(){
        // TODO: Group must be in current organization
        $groups = $this->groups;
        $allGroups = [];
        foreach($groups as $group){
            // First, check that the top level group is in the current organization
            $groupOrg = $group->org();
            if(is_null($this->currentOrganization) && !is_null($groupOrg)) continue;
            if(!is_null($this->currentOrganization) && is_null($groupOrg)) continue;
            if(!is_null($this->currentOrganization) && !is_null($groupOrg) && $this->currentOrganization->id != $groupOrg->id) continue;

            $allGroups[$group->id] = $group->name;
            $parent = $group->parent;
            if(is_null($parent)) continue;
            $searching = true;
            while($searching){
                if(array_key_exists($parent->id,$allGroups)) abort(500,"Circular group detected");
                $allGroups[$parent->id] = $parent->name;
                $parent = $parent->parent;
                if(is_null($parent)) $searching = false;
            }
        }
        return $groups;
    }
    
    public function groupPermissions(Group $group){
        $permissions = [];
        foreach($this->roles as $role){
            if(is_null($role->pivot->group_id) || $role->pivot->group_id != $group->id) continue;
            $permissions = array_merge($permissions,$role->allperms());
        }
        return $permissions;
    }

    public function groupsWherePermission($desired){
        $groups = $this->resolveGroups();
        $desiredGroups = [];
        foreach($this->groups as $group){
            $roles = $this->groupPermissions($group);
            if((is_string($desired) && in_array($desired,$roles)) || (is_array($desired) && !empty(array_intersect($desired,$roles)))){
                $desiredGroups[] = $group->id;
            }
        }
        return $desiredGroups;
    }

    public function inGroup($id){
        return array_key_exists($id, $this->resolveGroups());
    }

    public function hasPerm($perm){
        $permissions = $this->permissions;
        if(is_array($perm)){
            foreach($perm as $p){
                if(in_array($p,$permissions)) return true;
            }
            return false;
        }
        return in_array($perm,$permissions);
    }

    public function hasSitePerm($perm){
        $permissions = $this->site_permissions;
        if(is_array($perm)){
            foreach($perm as $p){
                if(in_array($p,$permissions)) return true;
            }
            return false;
        }
        return in_array($perm,$permissions);
    }

    public function hasGroupPerm(Group $group, $perm){
        $groupPerms = $this->groupPermissions($group);
        if(is_array($perm)){
            foreach($perm as $p){
                if(in_array($p,$groupPerms)) return true;
            }
            return false;
        }
        return in_array($perm,$groupPerms);
    }

    public function hasParentGroupPerm(Group $group, $perm){
        if($this->hasGroupPerm($group, $perm)) return true;
        while (!is_null($group->parent)) {
            $group = $group->parent;
            if($this->hasGroupPerm($group, $perm)) return true;
        }
        return false;
    }
}
