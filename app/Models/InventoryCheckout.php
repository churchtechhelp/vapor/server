<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventoryCheckout extends Model
{
    use HasFactory;

    protected $with = [
        'inventory'
    ];

    public function inventory(){
        return $this->belongsTo('App\Models\Inventory');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function approver(){
        return $this->belongsTo('App\Models\User','approved_user_id');
    }

    public function checkoutUser(){
        return $this->belongsTo('App\Models\User','checkout_user_id');
    }

    public function checkinUser(){
        return $this->belongsTo('App\Models\User','checkin_user_id');
    }
}
