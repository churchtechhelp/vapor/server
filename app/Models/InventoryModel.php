<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventoryModel extends Model
{
    use HasFactory;

    public function instances(){
        return $this->hasMany('App\Models\Inventory');
    }

    public function type(){
        return $this->belongsTo('App\Models\InventoryType');
    }

    public function model(){
        return $this->belongsTo('App\Models\InventoryModel');
    }
}
