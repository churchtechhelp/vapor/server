<?php

namespace App\Models;

class Organization extends GroupBase {
    protected $attributes = [
        'is_organization' => true,
    ];

    public static function booted(){
        static::addGlobalScope(function ($query) {
            $query->where('is_organization', true);
        });
    }

    public function getFeaturesAttribute(){
        return explode(';',$this->attributes['features']);
    }

    public function setFeaturesAttribute($value){
        if(is_array($value)) $this->attributes['features'] = implode(';',$value);
        elseif(is_string($value) && strpos($value,',') === false) $this->attribute['features'] = $value;
        else throw new Exception("Wrong type of value given for features");
    }
}