<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    use HasFactory;

    protected $casts = [
        'config' => 'array',
    ];

    public function events(){
        return $this->morphMany('App\Models\Event', 'target');
    }

    public function archiveUser(){
        return $this->belongsTo('App\Models\User', 'archived_user_id');
    }

    public function creator(){
        return $this->belongsTo('App\Models\User', 'creator_user_id');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User');
    }

    public function group(){
        return $this->belongsTo('App\Models\Group');
    }

    public function files(){
        return $this->belongsToMany('App\Models\File');
    }

    public function displays(){
        return $this->hasMany('App\Models\Display');
    }

    public function touchEvents(){
        return $this->hasMany('App\Models\ShowTouchEvent');
    }
}
