<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Events\GroupDeleted;

class GroupBase extends Model
{
    use HasFactory;

    protected $table = 'groups';

    protected $dispatchesEvents = [
        'deleted' => GroupDeleted::class,
    ];

    public function children(){
        if($this->is_organization) return $this->hasMany('App\Models\Group', 'organization_id')->whereNull('parent_id');
        return $this->hasMany('App\Models\Group', 'parent_id');
    }

    public function allChildren(){
        //recursive CTE with ability to sort using depth
        return \DB::select("WITH RECURSIVE traverse AS (
            SELECT *, 0 depth FROM groups
            WHERE parent_id = ?
            UNION ALL
            SELECT groups.*, traverse.depth + 1 FROM groups
            INNER JOIN traverse
            ON groups.parent_id = traverse.id
        )
        SELECT * from traverse
        ",[$this->id]);
    }

    public function link(){
        if($this->is_organization) return '/organizations/' . $this->attributes['id'];
        return '/groups/' . $this->attributes['id'];
    }

    public function primaryUser(){
        return $this->belongsTo('App\Models\User', 'primary_user_id');
    }

    public function events(){
        return $this->morphMany('App\Models\Event', 'target');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User','group_user','group_id','user_id');
    }

    public function shows(){
        return $this->hasMany('App\Models\Show');
    }

    public function displays(){
        return $this->hasMany('App\Models\Display');
    }

    public function displayGroups(){
        return $this->hasMany('App\Models\DisplayGroups');
    }

    /**
     * Roles owned by the group/organization
     */
    public function roles(){
        return $this->hasMany('App\Models\Role');
    }

    // EVENTS
    
}
