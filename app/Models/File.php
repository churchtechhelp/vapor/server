<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    protected $appends = ['structure'];

    public function events(){
        return $this->morphMany('App\Models\Event', 'target');
    }

    public function owner(){
        return $this->belongsTo('App\Models\User', 'owner_id');
    }

    public function shows(){
        return $this->belongsToMany('App\Models\Show');
    }

    public function groups(){
        return $this->belongsToMany('App\Models\Group');
    }

    public function getStructureAttribute(){
        if($this->type == 2) {
            return $this->getDirectoryStructure(\Storage::disk('public')->path($this->path));
        }
        return null;
    }

    private function getDirectoryStructure($dir, $origdir = "", &$results = array()){
        // From: https://stackoverflow.com/questions/24783862/list-all-the-files-and-folders-in-a-directory-with-php-recursive-function
        if($origdir === "") $origdir = $dir;
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = substr($path,strlen($origdir));
            } else if ($value != "." && $value != "..") {
                $this->getDirectoryStructure($path, $origdir, $results);
                $results[] = substr($path,strlen($origdir));
            }
        }
    
        return $results;
    }
}
