<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'permissions',
        'description',
        'group_linked',
    ];

    public function users(){
        return $this->belongsToMany('App\Models\User');
    }

    public function has($role) {
        return in_array($role, $this->permissions);
    }

    public function getPermissionsAttribute($perms){
        return explode(';',$perms);
    }

    public function allperms(){
        $perms = $this->permissions;
        $expanded = [];
        foreach($perms as $perm){
            if(array_key_exists($perm, config('permissions.aliases'))){
                $expanded = array_merge($expanded ,config('permissions.aliases.'.$perm));
            } else {
                $expanded[] = $perm;
            }
        }
        return $expanded;
    }

    public function setPermissionsAttribute($value){
        if(is_array($value)) $this->attributes['permissions'] = implode(';',$value);
        elseif(is_string($value) && strpos($value,',') === false) $this->attribute['permissions'] = $value;
        else throw new Exception("Wrong type of value given for permission");
    }
}
