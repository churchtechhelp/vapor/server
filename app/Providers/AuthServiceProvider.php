<?php

namespace App\Providers;

use Auth;
use App\Services\Auth\DisplayUserProvider;
use App\Services\Auth\DisplayGuard;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Policies\FilePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('display', function($app, array $config) {
            return new DisplayUserProvider;
        });
        Auth::extend('display', function ($app, $name, array $config){
            return new DisplayGuard($app['request'], new DisplayUserProvider);
        });

        // Gates/Policies
        Gate::define('view-file', [FilePolicy::class, 'view']);
        Gate::define('view-file-group', [FilePolicy::class, 'viewGroup']);
        Gate::define('edit-file', [FilePolicy::class, 'edit']);
        Gate::define('edit-file-group', [FilePolicy::class, 'editGroup']);
    }
}

