<?php

namespace App\Traits;

trait Loggable {
    public $object;
    public $class;
    public $user;
}