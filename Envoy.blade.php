@servers(['prod' => 'deployer@vaporserver.churchtech.help'])

@setup
    $repository = 'git@gitlab.com:churchtechhelp/vapor/server.git';
    $releases_dir = '/var/www/vapor/releases';
    $app_dir = '/var/www/vapor';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir . '/' . $release;
@endsetup

@story('deploy')
    clone
    install
    links
    migrate
    clean
    live
@endstory

@task('clone')
    echo 'Cloning Repo'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('install')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
    npm install --silent
    npm run production
@endtask

@task('clean')
    cd {{ $new_release_dir }}
    echo "Clearing PHP OpCache"
    if [ -f ~/.composer/vendor/bin/cachetool ]; then
        ~/.composer/vendor/bin/cachetool opcache:reset --fcgi=/run/php/php7.4-fpm.sock
    elif [ -f ~/.config/composer/vendor/bin/cachetool ]; then
        ~/.config/composer/vendor/bin/cachetool opcache:reset --fcgi=/run/php/php7.4-fpm.sock
    else
        exit 1
    fi
@endtask

@task('links')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage/app/public {{ $new_release_dir }}/public/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
@endtask

@task('migrate')
    echo "Running migrations"
    cd {{ $new_release_dir }}
    php artisan migrate --force
@endtask

@task('live')
echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask
