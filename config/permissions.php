<?php

/*
    This file is used for generating the list of permissions in the role configuration tool.
    BE CAREFUL, as you can compromise your security if you put a site-level permission in the groups.
    Please note that everything you put here will be passed to the Role Manager page.
*/

return [
    'site' => [ // only site admins can see these
        'admin' => 'Administrator',
        'write-anything' => 'Write Anything', // write-all?
        'read-anything' => 'Read Anything', // read everything on site
        'read-orgs' => 'Read Organizations',
        'write-orgs' => 'Write Organizations',
        'read-groups' => 'Read Groups',
        'write-groups' => 'Write Groups',
        'read-roles' => 'Read Roles',
        'write-roles' => 'Write Roles',
        'read-users' => 'Read Users',
        'write-users' => 'Write Users',
        'read-displays' => 'Read displays',
        'write-displays' => 'Write displays',
        'read-shows' => 'Read Shows',
        'write-shows' => 'Write Shows',
    ],
    'group' => [ // Group (and site) specific permissions. Group admins can create roles from these.
        'admin' => 'Group Administrator', // group membership, etc.
    ],
    'aliases' => [ // Permissions that will be resolved to other permissions. Add to site/group to view in role manager.
        'admin' => ['write-anything','read-anything'],
    ],
];