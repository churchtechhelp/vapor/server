@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'form-input rounded-md shadow-sm dark:shadow-light-sm dark:bg-gray-900 dark:text-white']) !!}>
