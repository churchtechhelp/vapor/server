<div class="flex flex-col flex-grow sm:justify-center items-center pt-6 sm:pt-0">
    <div>
        {{ $logo }}
    </div>

    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white dark:bg-gray-600 shadow-md dark:shadow-light-md overflow-hidden sm:rounded-lg">
        {{ $slot }}
    </div>
</div>
