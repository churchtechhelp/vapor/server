<x-guest-layout>
    <x-authentication-card>
        <x-slot name="logo">
            <x-app-logo />
        </x-slot>

        <x-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}" id="reg-form">
            @csrf

            <div>
                <x-label value="{{ __('Name') }}" />
                <x-input class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-label value="{{ __('Email') }}" />
                <x-input class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <x-label value="{{ __('Password') }}" />
                <x-input class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-label value="{{ __('Confirm Password') }}" />
                <x-input class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="mt-4 dark:text-white">
                By using this website, you agree to our <a href='{{ route('terms') }}' target="_BLANK" class="text-blue-800 dark:text-blue-400 underline">terms and conditions</a>.
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-gray-100" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <button type='submit' class='ml-4 inline-flex items-center px-4 py-2 bg-gray-800 dark:bg-gray-200 border-2 border-transparent dark:border-gray-800 dark:font-bold rounded-md font-semibold text-xs text-white dark:text-gray-900 uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 g-recaptcha'
                    data-sitekey="{{ env('RECAPTCHA_CLIENT_SECRET','') }}"
                    data-callback="onSubmit"
                    data-action="submit"
                >
                    {{ __('Register') }}
                </button>
            </div>
        </form>
    </x-authentication-card>
    @if(env('RECAPTCHA_CLIENT_SECRET',false))
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        function onSubmit(token) {
            document.getElementById("reg-form").submit();
        }
    </script>
    @endif
</x-guest-layout>
