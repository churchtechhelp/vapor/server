<x-guest-layout>
    <x-authentication-card>
        <x-slot name="logo">
            <x-app-logo />
        </x-slot>

        <x-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}" id="login-form">
            @csrf

            <div>
                <x-label value="{{ __('Email') }}" />
                <x-input class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-label value="{{ __('Password') }}" />
                <x-input class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label class="flex items-center">
                    <input type="checkbox" class="form-checkbox" name="remember">
                    <span class="ml-2 text-sm text-gray-600 dark:text-gray-400">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-gray-100 mr-2" href="{{ route('register') }}">
                    New? Register Here
                </a>
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-gray-100" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <button type='submit' class='ml-4 inline-flex items-center px-4 py-2 bg-gray-800 dark:bg-gray-200 border-2 border-transparent dark:border-gray-800 rounded-md font-semibold text-xs text-white dark:text-gray-900 dark:font-bold uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 g-recaptcha'
                    data-sitekey="{{ env('RECAPTCHA_CLIENT_SECRET','') }}"
                    data-callback="onSubmit"
                    data-action="submit"
                >
                    {{ __('Login') }}
                </button>
            </div>
        </form>
    </x-authentication-card>
    @if(env('RECAPTCHA_CLIENT_SECRET',false))
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        function onSubmit(token) {
            document.getElementById("login-form").submit();
        }
    </script>
    @endif
</x-guest-layout>
