<x-guest-layout>
    <div class="px-24 py-12 bg-gray-200 min-h-screen">
        <div class="grid grid-cols-1 border border-black rounded justify-center px-2 py-1 bg-white">
            <div class="flex w-full justify-center">
                <img src="/logo-long.svg">
            </div>
            <div class="flex text-4xl font-bold justify-center">
                Vapor Licensing
            </div>
            <div class="block text-xl font-semibold mb-2">Code License</div>
            <div class="block mb-3 ml-2">
                The Visual Arts Portable Digital Graphics System, e.g. Vapor, is copywrighted under the GNU GPLv3 license.
                Under the terms of the GPL license, code must be available and may not be used in proprietary applications.
                The code for this project is available on <a href="https://gitlab.com/churchtechhelp/vapor/server">GitLab</a>.
            </div>
            <div class="block text-xl font-semibold mb-2">Terms and Conditions</div>
            <div class="ml-2">
                <div class="block mb-1">
                The following terms and conditions govern all use of the Vapor website (the "Website").
                The Website is offered subject to your acceptance without modification of all the terms and conditions
                contained herin and all other operating rules, policies, and procedures that may be published from time to time on this Website by Church Tech Help (collectively, the "Agreement").
                </div>
                <div class="block mb-1">
                Please read this Agreement carefully before accessing or using the Website.
                By accessing or using any part of the Website, you agree to be bound by the terms and conditions of this Agreement.
                If you do not agree to all the terms and conditions of this Agreement, then you may not access the Website or use any of the services.
                The Website is available only to individuals who are a part of a non-profit organization or church who are at least 13 years old.
                </div>
                <div class="mb-1 text-lg">Your Account Responsibilities</div>
                <div class="block mb-1 ml-2">
                If you create an account on this Website, you are responsible for maintaining the security of your account.
                You are responsible for all activites that occur under the account and any other actions taken in connection with the account.
                You must immediately notify Church Tech Help of any unauthorized use of your account or any other breaches of security.
                Church Tech Help will not be liable for any acts or omissions by You, including any damages of any kind incurred as a result of such acts or omissions.
                </div>
                <div class="mb-1 text-lg">Acceptible Use</div>
                <div class="block mb-1 ml-2">
                By accepting this Agreement you agree not to use, encourage, promote, or facilitate others to use
                the Website or your account in any way that is harmful to others.
                Examples of harmful use include, but are not limited to:
                <ul class="list-disc ml-6">
                    <li class="my-1">Engaging in illegal or fradulent activities.</li>
                    <li class="mb-1">Infringing upon others' intellectual property rights</li>
                    <li class="mb-1">Distributing harmful or offensive content that is defamatory, obscene, abusive, an invasion of privacy or harassing.</li>
                    <li class="mb-1">Distributing copywrighted content</li>
                    <li class="mb-1">Distributing malicious content or spam</li>
                    <li class="mb-1">Taxing Church Tech Help resources with activites such as cryptocurrency</li>
                </ul>
                </div>
                <div class="mb-1 text-lg">Privacy</div>
                <div class="block mb-1">
                Church Tech Help respects your privacy, but has not yet decided what measures or agreements to put in this Agreement.
                Site Administrators reserve the right to view or remove any content uploaded to the site, but will only
                do so if asked by the owner of the content or if suspicious activity warrants it.
                </div>
                <div class="block mb-1 ml-2">
                Church Tech Help leverages site analytics in order to improve the Website.
                </div>
                <div class="block mb-1 ml-2">
                If you are a part of the EU and wish to exercise your right of erasure, please email Church Tech Help.
                </div>
                <div class="mb-1 text-lg">Termination</div>
                <div class="block mb-1 ml-2">
                Church Tech Help may terminate your access to all or any part of the Website at any time, with or without cause, with or without notice, effective immediately.
                If you wish to terminate this Agreement or your GitLab account, you may simply discontinue using the Website.
                All provisions of this Agreement which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
                </div>
                <div class="mb-1 text-lg">Disclaimer of Warranty</div>
                <div class="block mb-1 ml-2">
                The Website is provided "as is".
                Church Tech Help makes no warranty that the Website will be error free or that access therto will be continuous or uninterrupted.
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>