<x-guest-layout>
    <div class="py-4 px-12">
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <h2 class="text-2xl mb-3">About Vapor DGS</h2>
            <p class="mb-2">
                Vapor DGS stands for Visual Arts Portable Digital Graphics System.
                It is capable of displaying video, images, and websites on remotely controlled displays.
                It features automated or interactive shows that can operate without the need for internet.
            </p>
            <p class="mb-2">
                There is still a lot of work to do to get it to where we desire, but it is working right now.
                If you are interested in helping, please see the project on <a class="text-blue-600 dark:text-blue-400" href="https://gitlab.com/churchtechhelp/vapor/server" target="_BLANK">GitLab</a>.
            </p>
            <p class="mb-2">
                You can host your own Vapor server for free, but the hosted version will require payment.
            </p>
            <p>
                For more information, please check out the main website, <a class="text-blue-600 dark:text-blue-400" href="https://churchtech.help/resources/software/inhouse/vapor.html">ChurchTech.Help</a>.
            </p>
        </div>
    </div>
</x-guest-layout>