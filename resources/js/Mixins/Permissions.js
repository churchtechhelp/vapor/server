export default {
    methods: {
        //'inventory' in $page.user.features || )
        hasPermission(permission){
            if(Array.isArray(permission)) return this.$page.user.permissions.some(perm => {
                if(perm.startsWith('group:')) {
                    // Checking if user has a * group permission in the array
                    return permission.indexOf("group:" + perm) >= 0
                }
                return permission.indexOf(perm) >= 0
            });
            // checking if user has a * group permission
            if(permission.startsWith('group:') && permission.split(':').length < 2) return this.$page.user.permissions.some(perm => {
                return perm.startsWith('group:') && perm.split(':',3)[2] == permission.split(":",2)[1];
            });
            return this.$page.user.permissions.includes(permission);
        },
        hasAnyPermission(permission){
            return this.$page.user.permissions.some(perm=>{
                // need to strip group:#:
                if(perm.startsWith('group:')) perm = perm.split(':')[2];
                if(Array.isArray(permission)) return permission.indexOf(perm) >= 0;
                return permission == perm;
            });
        },
        ownsFeature(feature){
            return this.$page.user.features.includes(feature);
        },
    },
}