<template>
<div>
    <app-layout>
        <template #header>Group: {{ group.name }}</template>
        <panel class="mb-4">
            <template #breadcrumbs>
                <inertia-link v-if="Object.values(breadcrumbs)[0].startsWith('/organizations')" href="/organizations" class="text-blue-500 font-bold">Organizations</inertia-link>
                <inertia-link v-else href="/groups" class="text-blue-500 font-bold">Groups</inertia-link>
                <div v-for="(link, name, i) in breadcrumbs" :key="name"><!-- TODO: Permissions may not allow all breadcrumbs -->
                    <span class="mx-2">/</span>
                    <inertia-link v-if="(Object.keys(breadcrumbs).length) > (i+1)" class="text-blue-500 font-bold" :href="link">{{name}}</inertia-link>
                    <span v-else>{{name}}</span>
                </div>
            </template>
            <div class="px-4 py-2">
                Owner: <inertia-link :href="'/users/' + group.primary_user.id">{{group.primary_user.name}}</inertia-link>
            </div>
        </panel>
        <div class="grid grid-cols-2 gap-2">
            <searchable-data-card
                    v-if="hasPermission(['read-anything','read-users','group:'+group.id+':read-users'])"
                    name="Users"
                    type="users"
                    action="add"
                    :form="userform"
                    :data="group.users"
                    :headers="{name:'Name',email:'Email'}"
                    :parent="'group/' + group.id"
                    @new="newUserModalOpen = true"
                    @click="userSelected"
                />
                
            <searchable-data-card
                v-if="hasPermission(['read-anything','read-groups','group:'+group.id+':read-groups'])"
                name="Sub-Groups"
                type="groups"
                :form="groupform"
                :data="group.children"
                :headers="{name:'Name'}"
                :parent="'group/' + group.id"
                @new="newGroupModalOpener()"
                @click="groupSelected"
            />
        </div>
    </app-layout>
    <dialog-modal :show="newGroupModalOpen" @close="newGroupModalOpen = false">
        <template #title>New Group</template>
        <div class="mb-4">
            <x-label class="block text-gray-700 text-sm font-bold mb-2">Name</x-label>
            <x-input v-model="groupform.name" placeholder="My Group" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"/>
        </div>
        <template #footer>
            <danger-button @click.native.prevent="newGroupModalOpen = false">Cancel</danger-button>
            <x-button class="ml-2" @click.native.prevent="createGroup">Create</x-button>
        </template>
    </dialog-modal>
    <dialog-modal :show="newUserModalOpen" @close="newUserModalOpen = false">
        <template #title>Add Users to {{organization.name}}</template>
        <div class="mb-4">
            <x-label class="block text-gray-700 text-sm font-bold mb-2">Emails</x-label>
            <x-input v-model="userform.emails" placeholder="example@example.org;example2@example.net" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"/>
            <div class="block text-gray-600 text-sm mt-1">Please separate emails by a space, comma, or semicolon</div>
        </div>
        <template #footer>
            <danger-button @click.native.prevent="newUserModalOpen = false">Cancel</danger-button>
            <x-button class="ml-2" @click.native.prevent="addUsers">Add</x-button>
        </template>
    </dialog-modal>
</div>
</template>

<script>
import SearchableDataCard from './../../Components/SearchableDataCard'
export default {
    components: {
        SearchableDataCard
    },
    props: {
        group: {
            type: Object,
            required: true,
        },
        breadcrumbs: {
            type: Object,
        }
    },
    data(){
        return {
            groupform: this.$inertia.form({
                name: this.name,
            }, {
                bag: 'newGroup',
                resetOnSuccess: true,
            }),
            userform: this.$inertia.form({
                emails: this.emails,
            }, {
                bag: 'addUser',
                resetOnSuccess: true,
            }),
            newGroupModalOpen: false,
            newUserModalOpen: false,
            featuresInProgress: [],
            selectedUser: null,
            selectedGroup: null,
        }
    },
    methods:{
        newGroupModalOpener(id = null){
            this.newGroupModalOpen = true;
            this.groupform.parent = id;
        },
        createGroup(){
            this.newGroupModalOpen = false;
            this.groupform.post('/groups/'+this.group.id+'/groups',{
                preserveScroll: true,
            });
        },
        addUsers(){
            this.newUserModalOpen = false;
            this.userform.post('/groups/'+this.group.id+'/users',{
                preserveScroll: true,
            });
        },
        groupSelected(id){
            this.$inertia.get('/groups/' + id);
        },
        userSelected(id){
            this.selectedUser = id;
        },
    }
}
</script>