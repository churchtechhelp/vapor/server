require('./bootstrap');

import Vue from 'vue';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';
import VueFeather from 'vue-feather';
import Permissions from './Mixins/Permissions';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSpinner, faDesktop, faFilm, faClipboardCheck, faUsers, faQuestion, faPlus, faTrash, faPencilAlt, faUpload, faCheck, faTabletAlt } from '@fortawesome/free-solid-svg-icons';
import { faApple, faRaspberryPi, faUbuntu, faWindows } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
    faSpinner,
    faDesktop,
    faFilm,
    faClipboardCheck,
    faUsers,
    faQuestion,
    faPlus,
    faTrash,
    faPencilAlt,
    faUpload,
    faCheck,
    faWindows,
    faUbuntu,
    faApple,
    faTabletAlt,
    faRaspberryPi,
);

Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);
Vue.use(VueFeather);
Vue.mixin(Permissions);
Vue.component('fa-icon', FontAwesomeIcon);

// Custom global components

import Panel             from './Components/Panel';
import SlideDropdown     from './Components/SlideDropdown'
import AppLayout         from './Layouts/AppLayout'
import DialogModal       from './Components/DialogModal'
import XButton           from './Components/XButton'
import SecondaryButton   from './Components/SecondaryButton'
import DangerButton      from './Components/DangerButton'
import ActionMessage     from './Components/ActionMessage'
import InputError        from './Components/InputError'
import ActionSection     from './Components/ActionSection'
import ConfirmationModal from './Components/ConfirmationModal'
import FormSection       from './Components/FormSection'
import XInput            from './Components/XInput'
import XLabel            from './Components/XLabel'
import SectionBorder     from './Components/SectionBorder'
import SectionTitle      from './Components/SectionTitle'
Vue.component('panel',             Panel            );
Vue.component('slide-dropdown',    SlideDropdown    );
Vue.component('app-layout',        AppLayout        );
Vue.component('dialog-modal',      DialogModal      );
Vue.component('x-button',          XButton          );
Vue.component('secondary-button',  SecondaryButton  );
Vue.component('danger-button',     DangerButton     );
Vue.component('action-message',    ActionMessage    );
Vue.component('input-error',       InputError       );
Vue.component('action-section',    ActionSection    );
Vue.component('confirmation-modal',ConfirmationModal);
Vue.component('form-section',      FormSection      );
Vue.component('x-input',           XInput           );
Vue.component('x-label',           XLabel           );
Vue.component('section-border',    SectionBorder    );
Vue.component('section-title',     SectionTitle     );

const app = document.getElementById('app');

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
}).$mount(app);
