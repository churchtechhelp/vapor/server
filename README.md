
# VAPOR DGS

## About Vapor

Vapor is a Laravel/PHP based Digital Graphics System.
It leverages Vue on the front end to give a modern development experience.
It is designed to run on Chrome.

## Server Setup

> The client only needs to be configured to point to your server.
> Use `vaporclient.churchtech.help?hostname=SERVERNAME&port=PORTNAME`, replacing SERVERNAME and PORTNAME with your server's setup.

1. Prepare the server
   - Storage: Plenty of space for videos, images, etc.
   - Memory/CPU: Not much right now until we start doing transcoding
   - Linux OS: **Ubuntu** preferred
   - Web Server - **Nginx** or Apache
   - PHP 7.3 or newer with BCMath, Ctype, Fileinfo, JSON, Mbstring, OpenSSL, PDO for your DB server, Tokenizer, XML, ZIP, GZ
   - Composer
   - NodeJS 12
   - Redis
   - Database (MySQL, **PostgreSQL**, SQL Server, SQLite)
   - Git
   - Supervisor
1. Prepare a SSL certificate for your server. Don't forget that Chrome uses the SAN, not CN.
1. Clone this repo. Checkout a release tag when available.
1. `cd` into the project folder
1. Copy `.env.example` to `.env`
1. Edit `.env` to match your setup
   - **APP_ENV**: If you are developing, choose dev, local, or test. *prod* uses HTTPS only.
   - HTTPS: **LARAVEL_WEBSOCKETS_SSL_LOCAL{CERT,PK}**: Set these to where your certificate and key are
   - **PUSHER_APP_\***: Make sure to setup the secret to something random.
   - **CACHE_DRIVER**, **QUEUE_CONNECTION**, **SESSION_DRIVER**: redis
1. Run `composer install` to install all PHP requirements
1. Run `npm install` to install all Node requirements
1. Run `npm run pprod` to create the frontend
1. Run `php artisan migrate` to setup database
1. Run `php artisan first-time` to setup roles and admin user
1. Setup Nginx - [Laravel Docs](https://laravel.com/docs/8.x/deployment#nginx) - Don't forget SSL setup
1. Setup Queue processor in supervisor - [Laravel Docs](https://laravel.com/docs/8.x/queues#supervisor-configuration)
1. Setup Websocket server in supervisor - runs `php artisan websockets:serve`
1. If you have a load-balancer, set it up to redirect websocket traffic to the websocket server port.

## Server Updates

1. Update code: `git pull` - if you are on a release, also run `git checkout <releasetag>`
1. Update PHP dependencies: `composer update`
1. Update node dependencies: `npm install`
1. Update frontend: `npm run pprod`
1. Update DB: `php artisan migrate`
