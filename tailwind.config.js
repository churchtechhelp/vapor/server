const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    future: {
        removeDeprecatedGapUtilities: true,
        purgeLayersByDefault: true,
    },
    purge: [
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            screens: {
                light: { raw: "(prefers-color-scheme: light)"},
                dark: { raw: "(prefers-color-scheme: dark)"},
            },
            listStyleType: {
                square: 'square',
                roman: 'roman',
            },
            boxShadow: {
                light: '0 1px 3px 0 rgba(200, 200, 200, 0.1), 0 1px 2px 0 rgba(200, 200, 200, 0.06)',
                'light-sm': '0 1px 2px 0 rgba(200, 200, 200, 0.05)',
                'light-md': '0 4px 6px -1px rgba(200, 200, 200, 0.1), 0 2px 4px -1px rgba(200, 200, 200, 0.06)',
                'light-lg': '0 10px 15px -3px rgba(200, 200, 200, 0.1), 0 4px 6px -2px rgba(200, 200, 252005, 0.05)',
                'light-xl': '0 20px 25px -5px rgba(200, 200, 200, 0.1), 0 10px 10px -5px rgba(200, 200, 200, 0.04)',
            },
            colors: {
                'gray-950': "#0B0F17",
            }
        },
    },

    variants: {
        opacity: ['responsive', 'hover', 'focus', 'disabled'],
    },

    plugins: [
        require('@tailwindcss/ui'),
    ],
};
